#!/bin/bash
source /software/root/v6.06.08/bin/thisroot.sh
source /cvmfs/sft.cern.ch/lcg/external/gcc/6.2/x86_64-slc6/setup.sh

export DYLD_LIBRARY_PATH=/software/lk13008/pythia8219/lib/:$DYLD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=/software/fastjet/v3.2.1/lib/:$DYLD_LIBRARY_PATH

export LD_LIBRARY_PATH=/software/lk13008/pythia8219/lib/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/software/fastjet/v3.2.1/lib/:$LD_LIBRARY_PATH
