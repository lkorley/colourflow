// main86.cc is a part of the PYTHIA event generator.
// Copyright (C) 2016 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This program is written by Stefan Prestel.
// It illustrates how to do UMEPS merging, see the Matrix Element
// Merging page in the online manual. An example command is
//     ./main86 main86.cmnd w_production hepmcout86.dat
// where main86.cmnd supplies the commands, w_production provides the
// input LHE events, and hepmcout86.dat is the output file. This
// example requires HepMC.

#include "Pythia8/Pythia.h"
//#include "Pythia8Plugins/HepMC2.h"
#include <unistd.h>

#include "fastjet/PseudoJet.hh"
#include "fastjet/ClusterSequence.hh"

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TVector3.h"

using namespace Pythia8;


bool colour_calc(Event *myevent, double pull_mags[],double pull_angles[],double pull_raps[],double pull_phis[],double pull_thetas[],
  double bjets_eta[],double bjets_phi[],double bjets_rap[],double bjets_pt[],double bjets_eta_mu[],double bjets_spread_eta[],
  double bjets_phi_mu[],double *bjets_M,int good_constituents[],bool *firstEvent,fastjet::JetDefinition *jetDef,
  std::vector <fastjet::PseudoJet> &fjInputs,const double kt_sizes,const bool Higg_Ev,const bool verboses)

//==========================================================================

// Example main programm to illustrate merging

int main( int argc, char* argv[] ){

  // Check that correct number of command-line arguments
  if (argc != 4) {
    cerr << " Unexpected number of command-line arguments ("<<argc<<"). \n"
         << " You are expected to provide the arguments" << endl
         << " 1. Input file for settings" << endl
         << " 2. Name of the input LHE file (with path), up to the '_tree'"
         << " identifier" << endl
         << " 3. Path for root tree file" << endl
         << " Program stopped. " << endl;
    return 1;
  }

  Pythia pythia;

  // Input parameters:
  pythia.readFile(argv[1]);

  /*
  // Interface for conversion from Pythia8::Event to HepMC one.
  HepMC::Pythia8ToHepMC ToHepMC;
  // Specify file where HepMC events will be stored.
  HepMC::IO_GenEvent ascii_io(argv[3], std::ios::out);
  // Switch off warnings for parton-level events.
  ToHepMC.set_print_inconsistency(false);
  ToHepMC.set_free_parton_exception(false);
  // Do not store cross section information, as this will be done manually.
  ToHepMC.set_store_pdf(false);
  ToHepMC.set_store_proc(false);
  ToHepMC.set_store_xsec(false);
  */

  // ROOT TTree initializattion
  // TTree name
  const TString outfilename = argv[3];
  const double kt_size      = 0.7;
  const bool verbose        = false;
  const bool Higgs_Ev       = false; //find way to handle this
  bool filltree;
  pythia.readString("ColourReconnection:mode = 1");

  // No event record printout.
  pythia.readString("Next:numberShowInfo = 0");
  pythia.readString("Next:numberShowProcess = 0");
  pythia.readString("Next:numberShowEvent = 0");

  // Fastjet analysis - select algorithm and parameters
  fastjet::Strategy               strategy = fastjet::Best;
  fastjet::RecombinationScheme    recombScheme = fastjet::E_scheme;
  fastjet::JetDefinition         *jetDef = NULL;
  jetDef = new fastjet::JetDefinition(fastjet::antikt_algorithm, kt_size,
    recombScheme, strategy);

  // Fastjet input
  std::vector <fastjet::PseudoJet> fjInputs;
  bool firstEvent = true;

  // Setup output tree
  double weight,evtweight,treeweight;
  double pull_mags[2],pull_angles[2],pull_raps[2],pull_phis[2],pull_thetas[2];
  double bjets_eta[2],bjets_phi[2],bjets_rap[2],bjets_pt[2],bjets_eta_mu[2];
  double bjets_spread_eta[2],bjets_phi_mu[2],bjets_M;
  int good_constituents[2];
  int nbjets=2,ngoodevents=0;
  TFile outputfile(outfilename,"RECREATE","RECREATE");
  TTree* tree = new TTree("T","T");
  //tree->Branch("ngoodevents",&ngoodevents,"ngoodevents/D");
  tree->Branch("nbjets",&nbjets,"nbjets/I");
  tree->Branch("bjets_eta",bjets_eta,"bjets_eta[nbjets]/D");
  tree->Branch("bjets_eta_mu",bjets_eta_mu,"bjets_eta_mu[nbjets]/D");
  tree->Branch("bjets_phi_mu",bjets_phi_mu,"bjets_phi_mu[nbjets]/D");
  tree->Branch("bjets_spread_eta",bjets_spread_eta,"bjets_spread_eta[nbjets]/D");
  tree->Branch("bjets_phi",bjets_phi,"bjets_phi[nbjets]/D");
  tree->Branch("bjets_rap",bjets_rap,"bjets_rap[nbjets]/D");
  tree->Branch("bjets_pt",bjets_pt,"bjets_pt[nbjets]/D");
  tree->Branch("pull_mags",pull_mags,"pull_mags[nbjets]/D");
  tree->Branch("pull_angles",pull_angles,"pull_angles[nbjets]/D");
  tree->Branch("pull_raps",pull_raps,"pull_raps[nbjets]/D");
  tree->Branch("pull_phis",pull_phis,"pull_phis[nbjets]/D");
  tree->Branch("pull_thetas",pull_thetas,"pull_thetas[nbjets]/D");
  tree->Branch("good_constituents",good_constituents,"good_constituents[nbjets]/I");
  tree->Branch("bjets_M",&bjets_M,"bjets_M/D");
  tree->Branch("weight",&treeweight,"weights/D");
  TH1D h_dR_1("h_dR_1","h_dR_1",30,0,3);
  TH1D h_dR_2("h_dR_2","h_dR_2",30,0,3);
  TH1D h_pT_rat_1("h_pT_rat_1","h_pT_rat_1",30,0,3);
  TH1D h_pT_rat_2("h_pT_rat_2","h_pT_rat_2",30,0,3);

  // Path to input events, with name up to the "_tree" identifier included.
  string iPath = string(argv[2]);

  // Number of events
  int nEvent = pythia.mode("Main:numberOfEvents");
  // Maximal number of additional LO jets.
  int nMaxLO =  pythia.mode("Merging:nJetMax");

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

  // Switch off all showering and MPI when estimating the cross section after
  // the merging scale cut.
  bool fsr = pythia.flag("PartonLevel:FSR");
  bool isr = pythia.flag("PartonLevel:ISR");
  bool mpi = pythia.flag("PartonLevel:MPI");
  bool had = pythia.flag("HadronLevel:all");
  pythia.settings.flag("PartonLevel:FSR",false);
  pythia.settings.flag("PartonLevel:ISR",false);
  pythia.settings.flag("HadronLevel:all",false);
  pythia.settings.flag("PartonLevel:MPI",false);

  // Switch on cross section estimation procedure.
  pythia.settings.flag("Merging:doXSectionEstimate", true);
  pythia.settings.flag("Merging:doUMEPSTree",true);

  int njetcounterLO = nMaxLO;
  string iPathTree  = iPath;// + "_tree";

  // Save estimates in vectors.
  vector<double> xsecLO;
  vector<double> nAcceptLO;

  cout << endl << endl << endl;
  cout << "Start estimating umeps tree level cross section" << endl;

  while(njetcounterLO >= 0) {

    // From njet, choose LHE file
    stringstream in;
    in   << "_" << njetcounterLO << ".lhe";
/*#ifdef GZIPSUPPORT
    if(access( (iPathTree+in.str()+".gz").c_str(), F_OK) != -1) in << ".gz";
#endif*/
    string LHEfile = iPathTree + in.str();

    pythia.settings.mode("Merging:nRequested", njetcounterLO);
    pythia.settings.mode("Beams:frameType", 4);
    pythia.settings.word("Beams:LHEF", LHEfile);
    pythia.init();

    // Start generation loop
    for( int iEvent=0; iEvent<nEvent; ++iEvent ){
      // Generate next event
      if( !pythia.next() ) {
        if( pythia.info.atEndOfFile() ){
          break;
        }
        else continue;
      }
      // check bpartons meet momentum cuts
    } // end loop over events to generate

    // print cross section, errors
    pythia.stat();

    xsecLO.push_back(pythia.info.sigmaGen());
    nAcceptLO.push_back(pythia.info.nAccepted());

    // Restart with ME of a reduced the number of jets
    if( njetcounterLO > 0 )
      njetcounterLO--;
    else
      break;

  } // end loop over different jet multiplicities

  // 
  // Switch off cross section estimation.
  pythia.settings.flag("Merging:doXSectionEstimate", false);

  // Switch showering and multiple interaction back on.
  pythia.settings.flag("PartonLevel:FSR",fsr);
  pythia.settings.flag("PartonLevel:ISR",isr);
  pythia.settings.flag("HadronLevel:all",had);
  pythia.settings.flag("PartonLevel:MPI",mpi);

  // Declare sample cross section for output.
  double sigmaTemp  = 0.;
  vector<double> sampleXStree;
  vector<double> sampleXSsubtTree;
  // Cross section an error.
  double sigmaTotal  = 0.;
  double errorTotal  = 0.;

  int sizeLO    = int(xsecLO.size());
  njetcounterLO = nMaxLO;
  iPathTree     = iPath;// + "_tree";

  while(njetcounterLO >= 0){

    // From njet, choose LHE file
    stringstream in;
    in   << "_" << njetcounterLO << ".lhe";
/*#ifdef GZIPSUPPORT
    if(access( (iPathTree+in.str()+".gz").c_str(), F_OK) != -1) in << ".gz";
#endif*/
    string LHEfile = iPathTree + in.str();

    pythia.settings.flag("Merging:doUMEPSTree",true);
    pythia.settings.flag("Merging:doUMEPSSubt",false);
    pythia.settings.mode("Merging:nRecluster",0);

    cout << endl << endl << endl
         << "Start tree level treatment for " << njetcounterLO << " jets"
         << endl;

    pythia.settings.mode("Merging:nRequested", njetcounterLO);
    pythia.settings.mode("Beams:frameType", 4);
    pythia.settings.word("Beams:LHEF", LHEfile);
    pythia.init();

    // Remember position in vector of cross section estimates.
    int iNow = sizeLO-1-njetcounterLO;

    // Start generation loop
    for( int iEvent=0; iEvent<nEvent; ++iEvent ){

      // Generate next event
      if( !pythia.next() ) {
        if( pythia.info.atEndOfFile() ) break;
        else continue;
      }

      // jet clustering and colour pull calc goes HERE
      filltree = colour_calc(&(pythia.event),pull_mags,pull_angles,pull_raps,pull_phis,pull_thetas,bjets_eta,bjets_phi,bjets_rap,
        bjets_pt,bjets_eta_mu,bjets_spread_eta,bjets_phi_mu,&bjets_M,good_constituents,&firstEvent,
        jetDef,fjInputs,kt_size,Higgs_Ev,verbose);
      if(!filltree)
        continue;
      

      // Get event weight(s).
      weight = pythia.info.mergingWeight();
      evtweight = pythia.info.weight();
      weight *= evtweight;
      // Do not print zero-weight events.
      if ( weight == 0. ) continue;

      // Construct new empty HepMC event.
      //HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();

      // Get correct cross section from previous estimate.
      double normhepmc = xsecLO[iNow] / nAcceptLO[iNow];
      treeweight = weight*normhepmc;
      // Set event weight
      //hepmcevt->weights().push_back(weight*normhepmc);
      // Fill HepMC event.
      //ToHepMC.fill_next_event( pythia, hepmcevt );

      // Add the weight of the current event to the cross section.
      sigmaTotal += weight*normhepmc;
      sigmaTemp  += weight*normhepmc;
      errorTotal += pow2(weight*normhepmc);

      // fill tree
      tree->Fill();

      // Report cross section to hepmc
      //HepMC::GenCrossSection xsec;
      //xsec.set_cross_section( sigmaTotal*1e9, pythia.info.sigmaErr()*1e9 );
      //hepmcevt->set_cross_section( xsec );
      // Write the HepMC event to file. Done with it.
      //ascii_io << hepmcevt;
      //delete hepmcevt;

    } // end loop over events to generate

    // print cross section, errors
    pythia.stat();
    // Save sample cross section for output.
    sampleXStree.push_back(sigmaTemp);
    sigmaTemp = 0.;

    // Restart with ME of a reduced the number of jets
    if( njetcounterLO > 0 )
      njetcounterLO--;
    else
      break;

  }

  cout << endl << endl << endl;
  cout << "Do UMEPS subtraction" << endl;

  int njetcounterLS   = nMaxLO;
  string iPathSubt    = iPath;// + "_tree";

  while(njetcounterLS >= 1){

    // From njet, choose LHE file
    stringstream in;
    in   << "_" << njetcounterLS << ".lhe";
/*#ifdef GZIPSUPPORT
    if(access( (iPathSubt+in.str()+".gz").c_str(), F_OK) != -1) in << ".gz";
#endif*/
    string LHEfile = iPathSubt + in.str();

    pythia.settings.flag("Merging:doUMEPSTree",false);
    pythia.settings.flag("Merging:doUMEPSSubt",true);
    pythia.settings.mode("Merging:nRecluster",1);

    cout << endl << endl << endl
         << "Start subtractive treatment for " << njetcounterLS << " jets"
         << endl;

    pythia.settings.mode("Merging:nRequested", njetcounterLS);
    pythia.settings.mode("Beams:frameType", 4);
    pythia.settings.word("Beams:LHEF", LHEfile);
    pythia.init();

    // Remember position in vector of cross section estimates.
    int iNow = sizeLO-1-njetcounterLS;

    // Start generation loop
    for( int iEvent=0; iEvent<nEvent; ++iEvent ){

      // Generate next event
      if( !pythia.next() ) {
        if( pythia.info.atEndOfFile() ) break;
        else continue;
      }
      // jet clustering and colour pull calc goes HERE
      filltree = colour_calc(&(pythia.event),pull_mags,pull_angles,pull_raps,pull_phis,pull_thetas,bjets_eta,bjets_phi,bjets_rap,
        bjets_pt,bjets_eta_mu,bjets_spread_eta,bjets_phi_mu,&bjets_M,good_constituents,&firstEvent,
        jetDef,fjInputs,kt_size,Higgs_Ev,verbose);
      if(!filltree)
        continue;

      // Get event weight(s).
      weight    = pythia.info.mergingWeight();
      evtweight = pythia.info.weight();
      weight *= evtweight;

      // Do not print zero-weight events.
      if ( weight == 0. ) continue;

      // Construct new empty HepMC event.
      //HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();

      // Get correct cross section from previous estimate.
      double normhepmc = -1*xsecLO[iNow] / nAcceptLO[iNow];
      treeweight = weight*normhepmc;

      // Set event weight
      //hepmcevt->weights().push_back(weight*normhepmc);
      // Fill HepMC event.
      //ToHepMC.fill_next_event( pythia, hepmcevt );

      // Add the weight of the current event to the cross section.
      sigmaTotal += weight*normhepmc;
      sigmaTemp  += weight*normhepmc;
      errorTotal += pow2(weight*normhepmc);

      // fill tree
      tree->Fill();

      // Report cross section to hepmc.
      //HepMC::GenCrossSection xsec;
      //xsec.set_cross_section( sigmaTotal*1e9, pythia.info.sigmaErr()*1e9 );
      //hepmcevt->set_cross_section( xsec );
      // Write the HepMC event to file. Done with it.
      //ascii_io << hepmcevt;
      //delete hepmcevt;

    } // end loop over events to generate

    // print cross section, errors
    // pythia.stat();
    // Save sample cross section for output.
    sampleXSsubtTree.push_back(sigmaTemp);
    sigmaTemp = 0.;

    // Restart with ME of a reduced the number of jets
    if( njetcounterLS > 1 )
      njetcounterLS--;
    else
      break;
  }

  h_dR_1.Write();
  h_dR_2.Write();
  h_pT_rat_1.Write();
  h_pT_rat_2.Write();
  // End of event loop.
  outputfile.Write();

  // Print cross section information.
  cout << endl << endl;
  cout << " *---------------------------------------------------*" << endl;
  cout << " |                                                   |" << endl;
  cout << " | Sample cross sections after UMEPS merging         |" << endl;
  cout << " |                                                   |" << endl;
  cout << " | Leading order cross sections (mb):                |" << endl;
  for (int i = 0; i < int(sampleXStree.size()); ++i)
    cout << " |     " << sampleXStree.size()-1-i << "-jet:  "
         << setw(17) << scientific << setprecision(6)
         << sampleXStree[i] << "                     |" << endl;
  cout << " |                                                   |" << endl;
  cout << " | Leading-order subtractive cross sections (mb):    |" << endl;
  for (int i = 0; i < int(sampleXSsubtTree.size()); ++i)
    cout << " |     " << sampleXSsubtTree.size()-1-i+1 << "-jet:  "
         << setw(17) << scientific << setprecision(6)
         << sampleXSsubtTree[i] << "                     |" << endl;
  cout << " |                                                   |" << endl;
  cout << " |---------------------------------------------------|" << endl;
  cout << " |---------------------------------------------------|" << endl;
  cout << " | Inclusive cross sections:                         |" << endl;
  cout << " |                                                   |" << endl;
  cout << " | UMEPS merged inclusive cross section:             |" << endl;
  cout << " |    " << setw(17) << scientific << setprecision(6)
       << sigmaTotal << "  +-  " << setw(17) << sqrt(errorTotal) << " mb "
       << "   |" << endl;
  cout << " |                                                   |" << endl;
  cout << " | LO inclusive cross section:                       |" << endl;
  cout << " |    " << setw(17) << scientific << setprecision(6)
       << xsecLO.back() << " mb                           |" << endl;
  cout << " |                                                   |" << endl;
  cout << " *---------------------------------------------------*" << endl;
  cout << endl << endl;


  delete jetDef;

  // Done
  return 0;

}


// Colour pull and jet clustering calculation
bool colour_calc(Event *myevent, double pull_mags[],double pull_angles[],double pull_raps[],double pull_phis[],double pull_thetas[],
  double bjets_eta[],double bjets_phi[],double bjets_rap[],double bjets_pt[],double bjets_eta_mu[],double bjets_spread_eta[],
  double bjets_phi_mu[],double *bjets_M,int good_constituents[],bool *firstEvent,fastjet::JetDefinition *jetDef,
  std::vector <fastjet::PseudoJet> &fjInputs,const double kt_sizes,const bool Higg_Ev,const bool verboses) {



      // Reset Fastjet input
      *fjInputs.resize(0);
      
      // Keep track of missing ET
      Vec4 missingETvec;
      
      // Keep track of b-quark parton
      vector<fastjet::PseudoJet> bpartons;
      
      // Keep track of jets matched to b-partons
      vector<fastjet::PseudoJet> bjets;

      // first look for the Higgs
      bool found_higgs = false,found_b_from_higgs=false,found_bb_from_higgs=false;
      if(Higg_Ev == true)
      {
        for(int ii=1;ii<=myevent->size();++ii) 
        {
          if(myevent[ii].id()!=25)
              continue;

          found_higgs=true;
          int d1 = myevent[ii].daughter1();
          int d2 = myevent[ii].daughter2();
          if(myevent[d1].id()==5&&found_b_from_higgs==false){
            bpartons.push_back(fastjet::PseudoJet( myevent[d1].px(),
              myevent[d1].py(),
              myevent[d1].pz(),
              myevent[d1].e() ));
            found_b_from_higgs=true;
          }
          if(myevent[d2].id()==-5&&found_bb_from_higgs==false){
            bpartons.push_back(fastjet::PseudoJet( myevent[d2].px(),
              myevent[d2].py(),
              myevent[d2].pz(),
              myevent[d2].e() ));
            found_bb_from_higgs=true;
          }
        }
      }
      if(Higg_Ev == false){
        if(myevent->size()<7){
          if(verboses)
            cout<<"INFO: Event seems tiny, will skip..."<<endl;
          continue;
        }
        for(int parties=1;parties<12;++parties){
          if(parties>=(myevent.size()-1)) break;
        if(myevent[parties].id()*myevent[parties+1].id()==-25 && // pseudorapidity restrictions
          myevent[parties].pT()>10.0&&myevent[parties+1].pT()>10.0){

          bpartons.push_back(fastjet::PseudoJet( myevent[parties].px(),
            myevent[parties].py(),
            myevent[parties].pz(),
            myevent[parties].e() ));
          bpartons.push_back(fastjet::PseudoJet( myevent[parties+1].px(),
            myevent[parties+1].py(),
            myevent[parties+1].pz(),
            myevent[parties+1].e() ));
        }
      }
      }
      if (bpartons.size() != 2){
        if(verboses){
          cout<<"INFO: event does not contain two well defined "
          <<"bquarks! Skipping..."<<iEvent<<endl;
        }
        return false;
          }
      
      // Loop over event record to decide what to pass to FastJet
      int fjInputsCounter=-1;
      for (int i = 0; i < myevent->size(); ++i)
      {
        // Final state only
        if (!myevent[i].isFinal())
          continue;
    
        // No neutrinos
        if (myevent[i].idAbs() == 12 || myevent[i].idAbs() == 14 ||
          myevent[i].idAbs() == 16)
          continue;
    
        // Only 2.0 < |eta| < 5.0
        //if ((myevent[i].eta()) < 2.0) continue;
        //if ((myevent[i].eta()) > 5.0) continue;
        //if (fabs(myevent[i].eta()) > 2.0) continue;
        if (myevent[i].pT() < 0.1)
            continue;
    
        // Missing ET
        missingETvec += myevent[i].p();
    
        // Store as input to Fastjet
        fjInputsCounter++;
        fastjet::PseudoJet fjinput( myevent[i].px(),
          myevent[i].py(),
          myevent[i].pz(),
          myevent[i].e() );
        fjinput.set_user_index(myevent[i].id());
        fjInputs.push_back(fjinput);
      }
      
    if (fjInputs.size() == 0) {
        cout << "ERROR: event with no final state particles! "
        << "Skipping event..."<<endl;
        return false;
    }
      
      // Run Fastjet algorithm
      vector <fastjet::PseudoJet> inclusiveJets, sortedJets;
      fastjet::ClusterSequence clustSeq(fjInputs, *jetDef);
      
      // For the first event, print the FastJet details
      if (*firstEvent) {
        cout << "Ran " << jetDef->description() << endl;
        cout << "Strategy adopted by FastJet was "
        << clustSeq.strategy_string() << endl << endl;
        *firstEvent = false;
      }
      
      // Extract inclusive jets sorted by pT (note minimum pT of 20.0 GeV)
      inclusiveJets = clustSeq.inclusive_jets(20.0);
      sortedJets    = sorted_by_pt(inclusiveJets);
      
      // Keep track of jets with pT > 15 GeV
      int  jetCount = 0;
      int imatch1=-1,imatch2=-1;
      for (unsigned int isj = 0; isj < sortedJets.size(); isj++)
      {
        // Only count jets that have 2.0 < |eta| < 5.0
        //if (fabs(sortedJets[isj].eta()) > 2.0) continue;
        //if (sortedJets[isj].eta() < 2.0) continue;
        //if (sortedJets[isj].eta() > 5.0) continue;

        // Fill dSigma histograms and count jets with ET > 20.0
        if (sortedJets[isj].perp() < 20.0) continue;
        jetCount++;

        // Match b partons to jets
        h_dR_1.Fill(sortedJets[isj].delta_R(bpartons[0]));
        h_dR_2.Fill(sortedJets[isj].delta_R(bpartons[1]));
        if(sortedJets[isj].delta_R(bpartons[0])<kt_sizes){
          bjets.push_back(sortedJets[isj]);
          h_pT_rat_1.Fill(sortedJets[isj].pt()/bpartons[0].pt());
          imatch1=isj;
        }
        if(sortedJets[isj].delta_R(bpartons[1])<kt_sizes){
          bjets.push_back(sortedJets[isj]);
          h_pT_rat_2.Fill(sortedJets[isj].pt()/bpartons[1].pt());
          imatch2=isj;
          }
      }
    
      if(jetCount < 2){
        if(verboses)
          cout<<"INFO: Less than 2 good jets found! Skipping event..."<<endl;
        return false;
      }
      
      if(imatch1==imatch2 || bjets.size()!=2){
        if(verboses)
          cout<<"ERROR: Something went wrong with the matching "
        <<"between bjets and bpartons! Skipping event..."<<endl;
        return false;
      }

      if(fabs(bjets[0].delta_phi_to(bjets[1]))<2.5){
        if(verboses)
          cout<<"INFO: b-jets are not back-to-back! "
        <<"Skipping event..."<<endl;
        return false;
          }
      
      
      // calculate angle between two b-jets
      bjets_eta[0] = bjets[0].eta();bjets_rap[0] = bjets[0].rapidity();
      bjets_eta[1] = bjets[1].eta();bjets_rap[1] = bjets[1].rapidity();
      bjets_pt[0]  = bjets[0].pt();bjets_pt[1]   = bjets[1].pt();
      bjets_phi[0] = bjets[0].phi();
      bjets_phi[1] = bjets[1].phi();
      bjets[0] += bjets[1];
      *bjets_M = bjets[0].m();
      bjets[0] -= bjets[1];
      TVector3 b1b2(bjets[0].rap()-bjets[1].rap(),
        bjets[1].delta_phi_to(bjets[0]),0.0);
      TVector3 b2b1 = -b1b2;
      double b1b2_angle = atan2(b1b2.Y(),b1b2.X());
      double b2b1_angle = atan2(b2b1.Y(),b2b1.X());

      // loop over constituents and calculate colour pull
      for (unsigned int ibj = 0; ibj < 2; ibj++)
      {
        // make sure constituent matches list of kaons/pions/protons
        pull_mags[ibj]         = 0;
        pull_angles[ibj]       = 0;
        pull_raps[ibj]         = 0;
        pull_phis[ibj]         = 0;
        pull_thetas[ibj]       = 0;
        good_constituents[ibj] = 0;
        bjets_spread_eta[ibj]  = 0;
        double jet_trk_sum_sq = 0,jet_pull_rap = 0,jet_pull_phi = 0, eta_mu = 0, eta_mu2 = 0;
        double phi_mu =0;
        vector<fastjet::PseudoJet> bjetconsts = bjets[ibj].constituents();
        for(size_t ibjc=0;ibjc<bjetconsts.size();++ibjc)
        {
          int idAbs=abs(bjetconsts[ibjc].user_index());
          if(idAbs!=211 && idAbs!=321 && idAbs!=2212 && idAbs!=11 && idAbs!=13)continue; //if not one of these then not detactable at lhcb
          //if(fabs(bjetconsts[ibjc].eta())>2.0)continue;
          //if(bjetconsts[ibjc].eta()<2.0)continue;
          //if(bjetconsts[ibjc].eta()>5.0)continue;
          //if(bjetconsts[ibjc].pt()<0.5)continue;
          good_constituents[ibj]++;
          double dR_trk_jet = bjetconsts[ibjc].delta_R(bjets[ibj]);
          double dE_trk_jet = bjetconsts[ibjc].rap()-bjets[ibj].rap();
          double dP_trk_jet = bjets[ibj].delta_phi_to(bjetconsts[ibjc]);// other.phi-phi
          phi_mu += dP_trk_jet;
          eta_mu += bjetconsts[ibjc].eta();
          eta_mu2 += bjetconsts[ibjc].eta()*bjetconsts[ibjc].eta();
          jet_trk_sum_sq += bjetconsts[ibjc].pt()*bjetconsts[ibjc].pt();
          jet_pull_rap   += (bjetconsts[ibjc].pt()*dR_trk_jet*dE_trk_jet/bjets_pt[ibj]);
          jet_pull_phi   += (bjetconsts[ibjc].pt()*dR_trk_jet*dP_trk_jet/bjets_pt[ibj]);
        }
        if(good_constituents[ibj]>2){
          bjets_phi_mu[ibj]     = phi_mu/(double)good_constituents[ibj];
          bjets_eta_mu[ibj]     = eta_mu/(double)good_constituents[ibj];
          bjets_spread_eta[ibj] = sqrt(eta_mu2/(double)good_constituents[ibj] - bjets_eta_mu[ibj]*bjets_eta_mu[ibj]);
          pull_mags[ibj]   = (sqrt(jet_pull_phi*jet_pull_phi+jet_pull_rap*jet_pull_rap));
          pull_angles[ibj] = (atan2(jet_pull_phi,jet_pull_rap));
          pull_raps[ibj]   = (jet_pull_rap);
          pull_phis[ibj]   = (jet_pull_phi);
          double bbangle;
          (ibj==0) ? bbangle = b2b1_angle : bbangle = b1b2_angle;
          pull_thetas[ibj] = pull_angles[ibj] - bbangle;
          if(pull_thetas[ibj]<-acos(-1))
            pull_thetas[ibj]+=2*acos(-1);
          if(pull_thetas[ibj]>=acos(-1))
            pull_thetas[ibj]-=2*acos(-1);
        }
      }
      if(good_constituents[0]<=2 || good_constituents[1]<=2){
        return false;
      }


 

  // Done.
  return true;
}