#include <TStyle.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVectorD.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TAxis.h>
#include <TTree.h>
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc,char* argv[]){
	//arg1 type of tree(s): {D=Data,MCU=MCUnrestricted,MCR=MCRestricted}
	//arg2 oldtreefilepath(s)

	TString newfilename;
	newfilename=TString(argv[1])+".root";
	TFile *newfile = new TFile(newfilename,"recreate");
   double jpx[2],jpy[2],jpz[2],je[2],jeta[2],jrap[2],jphi[2],jpt[2],jet[2],jm[2];
   double jspx,jspy,jspz,jse,jseta,jsrap,jsphi,jspt,jset,jsm,weight;
    double jpull[2];


	TString mytname;
	mytname = TString(argv[1])+"T1";
	TTree *mynewtree = new TTree(mytname,mytname);
	mynewtree->Branch("weight",&weight,"weight/D");
	mynewtree->Branch("px",jpx,"jpx[2]/D");
	mynewtree->Branch("py",jpy,"jpy[2]/D");
	mynewtree->Branch("pz",jpz,"jpz[2]/D");
	mynewtree->Branch("e",je,"je[2]/D");
	mynewtree->Branch("eta",jeta,"jeta[2]/D");
	mynewtree->Branch("rap",jrap,"jrap[2]/D");
	mynewtree->Branch("phi",jphi,"jphi[2]/D");
	mynewtree->Branch("pt",jpt,"jpt[2]/D");
	mynewtree->Branch("et",jet,"jet[2]/D");
	mynewtree->Branch("m",jm,"jm[2]/D");


	mynewtree->Branch("j1j2px",&jspx,"jspx/D");
	mynewtree->Branch("j1j2py",&jspy,"jspy/D");
	mynewtree->Branch("j1j2pz",&jspz,"jspz/D");
	mynewtree->Branch("j1j2e",&jse,"jse/D");
	mynewtree->Branch("j1j2eta",&jseta,"jseta/D");
	mynewtree->Branch("j1j2rap",&jsrap,"jsrap/D");
	mynewtree->Branch("j1j2phi",&jsphi,"jsphi/D");
	mynewtree->Branch("j1j2pt",&jspt,"jspt/D");
	mynewtree->Branch("j1j2et",&jset,"jset/D");
	mynewtree->Branch("j1j2m",&jsm,"jsm/D");

	mynewtree->Branch("theta",jpull,"jpull[2]/D");

	if(TString(argv[1]).Contains("D")){
			UInt_t npv;
			double gfraction_pt[2];
   		mynewtree->Branch("nPVs",&npv,"npv/i");
   		mynewtree->Branch("ptfraction",gfraction_pt,"gfraction_pt[2]/D");
   		TFile *myfile = new TFile(argv[2],"READ");
   		TTree *mytree = (TTree*)myfile->Get("T");
   		Long64_t nentries = mytree->GetEntries();
   		mytree->SetBranchAddress("nPVs",&npv);
   		mytree->SetBranchAddress("j1px",&(jpx[0]));
   		mytree->SetBranchAddress("j2px",&(jpx[1]));
   		mytree->SetBranchAddress("j1py",&(jpy[0]));
   		mytree->SetBranchAddress("j2py",&(jpy[1]));
   		mytree->SetBranchAddress("j1pz",&(jpz[0]));
   		mytree->SetBranchAddress("j2pz",&(jpz[1]));
   		mytree->SetBranchAddress("j1e",&(je[0]));
   		mytree->SetBranchAddress("j2e",&(je[1]));

   		double jptmu[2],ntrks[2];

   		mytree->SetBranchAddress("j1mu_trk_pt",&(jptmu[0]));
   		mytree->SetBranchAddress("j2mu_trk_pt",&(jptmu[1]));
   		mytree->SetBranchAddress("j1ntrks",&(ntrks[0]));
   		mytree->SetBranchAddress("j2ntrks",&(ntrks[1]));

   		mytree->SetBranchAddress("j1theta",&(jpull[0]));
   		mytree->SetBranchAddress("j2theta",&(jpull[1]));
   		for (Long64_t i=0;i<nentries; i++) {
      	mytree->GetEntry(i);
      	if(npv!=1)
   				continue;
      	weight=1;
      	jpx[0]*=1e-3;jpy[0]*=1e-3;jpz[0]*=1e-3;je[0]*=1e-3;
      	jpx[1]*=1e-3;jpy[1]*=1e-3;jpz[1]*=1e-3;je[1]*=1e-3;
      	jpull[0]*=1/acos(-1.0); jpull[1]*=1/acos(-1.0);
				jpull[0]=fabs(jpull[0]); jpull[1]=fabs(jpull[1]);
				TLorentzVector j1j2(jpx[0]+jpx[1],jpy[0]+jpy[1],jpz[0]+jpz[1],je[0]+je[1]);
    	  jspx=j1j2.Px();
    	 	jspy=j1j2.Py();
    	 	jspz=j1j2.Pz();
    	  jse=j1j2.E();
    	  jsrap=j1j2.Rapidity();
    	 	jseta=j1j2.Eta();
     	 	jsphi=j1j2.Phi();
     	 	jset=j1j2.Et();
      	jspt=j1j2.Pt();
     	 	jsm=j1j2.M();

      	TLorentzVector j1(jpx[0],jpy[0],jpz[0],je[0]);
      	TLorentzVector j2(jpx[1],jpy[1],jpz[1],je[1]);
      	jphi[0]=j1.Phi();
      	jphi[1]=j2.Phi();
      	jrap[0]=j1.Rapidity();
      	jrap[1]=j2.Rapidity();
      	jeta[0]=j1.Eta();
      	jeta[1]=j2.Eta();
     		jet[0]=j1.Et();
      	jet[1]=j2.Et();
      	jpt[0]=j1.Pt();
      	jpt[1]=j2.Pt();
      	jm[0]=j1.M();
      	jm[1]=j2.M();
      	gfraction_pt[0]=jptmu[0]*ntrks[0]/jpt[0];
      	gfraction_pt[1]=jptmu[1]*ntrks[1]/jpt[1];

      	if(jptmu[0]*ntrks[0]<0.6*jpt[0] || jptmu[1]*ntrks[1]<0.6*jpt[1])
      		continue;

      	mynewtree->Fill();
      }
     	myfile->Close();
	}
	else if(TString(argv[1]).Contains("MCU")){
		double gfraction_pt[2],jdR[2],jcpull[2],wfactor;
		int nExtraJets;

		mynewtree->Branch("theta_PID",jcpull,"jcpull[2]/D");
		mynewtree->Branch("dR_match",jdR,"jdR[2]/D");
		mynewtree->Branch("nExtraJets",&nExtraJets,"nExtraJets/I");
		mynewtree->Branch("ptfraction",gfraction_pt,"gfraction_pt[2]/D");
		mynewtree->Branch("wfactor",&wfactor,"wfactor/D");

		TFile *myfile = new TFile(argv[2],"READ");
   	TTree *mytree = (TTree*)myfile->Get("T");
		Long64_t nentries = mytree->GetEntries();
		mytree->SetBranchAddress("bjets_px",jpx);
    mytree->SetBranchAddress("bjets_py",jpy);
    mytree->SetBranchAddress("bjets_pz",jpz);
    mytree->SetBranchAddress("bjets_e",je);
    mytree->SetBranchAddress("pull_thetas",jcpull);
    mytree->SetBranchAddress("cpull_thetas",jpull);
    mytree->SetBranchAddress("treeweight",&weight);
    mytree->SetBranchAddress("UMEPSfactor",&wfactor);
    mytree->SetBranchAddress("cgood_fraction_pt",gfraction_pt);
    mytree->SetBranchAddress("bjets_dR",jdR);
    mytree->SetBranchAddress("nExtraJets",&nExtraJets);


		for (Long64_t i=0;i<nentries; i++) {
      	mytree->GetEntry(i);
      	if(wfactor==0)
      		continue;
      	/*jpx[0]*=1e-3;jpy[0]*=1e-3;jpz[0]*=1e-3;je[0]*=1e-3;
      	jpx[1]*=1e-3;jpy[1]*=1e-3;jpz[1]*=1e-3;je[1]*=1e-3;*/
      	int ibjmatch[2];
      	double jpt1=(jpx[0]*jpx[0]+jpy[0]*jpy[0]);
      	double jpt2=(jpx[1]*jpx[1]+jpy[1]*jpy[1]);
      	if(jpt1>=jpt2){
      		ibjmatch[0]=0;
      		ibjmatch[1]=1;
      	}
      	else{
      		ibjmatch[0]=1;
      		ibjmatch[1]=0;
      	}
      	double jtemp[2];
      	jtemp[0]=jpx[0]; jtemp[1]=jpx[1];
      	jpx[0]=jtemp[ibjmatch[0]];
      	jpx[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpy[0]; jtemp[1]=jpy[1];
      	jpy[0]=jtemp[ibjmatch[0]];
      	jpy[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpz[0]; jtemp[1]=jpz[1];
      	jpz[0]=jtemp[ibjmatch[0]];
      	jpz[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=je[0]; jtemp[1]=je[1];
      	je[0]=jtemp[ibjmatch[0]];
      	je[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpull[0]; jtemp[1]=jpull[1];
      	jpull[0]=jtemp[ibjmatch[0]];
      	jpull[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jcpull[0]; jtemp[1]=jcpull[1];
      	jcpull[0]=jtemp[ibjmatch[0]];
      	jcpull[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jdR[0]; jtemp[1]=jdR[1];
      	jdR[0]=jtemp[ibjmatch[0]];
      	jdR[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=gfraction_pt[0]; jtemp[1]=gfraction_pt[1];
      	gfraction_pt[0]=jtemp[ibjmatch[0]];
      	gfraction_pt[1]=jtemp[ibjmatch[1]];
      	
    	  TLorentzVector j1j2(jpx[0]+jpx[1],jpy[0]+jpy[1],jpz[0]+jpz[1],je[0]+je[1]);
    	  jspx=j1j2.Px();
    	 	jspy=j1j2.Py();
    	 	jspz=j1j2.Pz();
    	  jse=j1j2.E();
    	  jsrap=j1j2.Rapidity();
    	 	jseta=j1j2.Eta();
     	 	jsphi=j1j2.Phi();
     	 	jset=j1j2.Et();
      	jspt=j1j2.Pt();
     	 	jsm=j1j2.M();

     	 	//jpull[0]*=1/acos(-1.0); jpull[1]*=1/acos(-1.0);
				jpull[0]=fabs(jpull[0])/acos(-1.0); jpull[1]=fabs(jpull[1])/acos(-1.0);
				//jcpull[0]*=1/acos(-1.0); jcpull[1]*=1/acos(-1.0);
				jcpull[0]=fabs(jcpull[0])/acos(-1.0); jcpull[1]=fabs(jcpull[1])/acos(-1.0);
      	TLorentzVector j1(jpx[0],jpy[0],jpz[0],je[0]);
      	TLorentzVector j2(jpx[1],jpy[1],jpz[1],je[1]);
      	jphi[0]=j1.Phi();
      	jphi[1]=j2.Phi();
      	jrap[0]=j1.Rapidity();
      	jrap[1]=j2.Rapidity();
      	jeta[0]=j1.Eta();
      	jeta[1]=j2.Eta();
     		jet[0]=j1.Et();
      	jet[1]=j2.Et();
      	jpt[0]=j1.Pt();
      	jpt[1]=j2.Pt();
      	jm[0]=j1.M();
      	jm[1]=j2.M();

      	if(gfraction_pt[0]<0.7 || jdR[0]>0.2)
      		continue;

      	mynewtree->Fill();
      }
     	myfile->Close();

	}
	else if(TString(argv[1]).Contains("MCR")){
		double gfraction_pt[2],jdR[2],jcpull[2],wfactor;
		int nExtraJets;

		mynewtree->Branch("theta_PID",jcpull,"jcpull[2]/D");
		mynewtree->Branch("dR_match",jdR,"jdR[2]/D");
		mynewtree->Branch("ptfraction",gfraction_pt,"gfraction_pt[2]/D");

		double pjpx[2],pjpy[2],pjpz[2],pje[2],pjeta[2],pjrap[2],pjphi[2],pjet[2],pjpt[2],pjm[2];
		double pjspx,pjspy,pjspz,pjse,pjseta,pjsrap,pjsphi,pjset,pjspt,pjsm;
		double jpullp[2],jcpullp[2],pulldiff,pgfraction_pt[2],pjdR[2];

  	mynewtree->Branch("p_px",pjpx,"pjpx[2]/D");
		mynewtree->Branch("p_py",pjpy,"pjpy[2]/D");
		mynewtree->Branch("p_pz",pjpz,"pjpz[2]/D");
		mynewtree->Branch("p_e",pje,"pje[2]/D");
		mynewtree->Branch("p_eta",pjeta,"pjeta[2]/D");
		mynewtree->Branch("p_rap",pjrap,"pjrap[2]/D");
		mynewtree->Branch("p_phi",pjphi,"pjphi[2]/D");
		mynewtree->Branch("p_pt",pjpt,"pjpt[2]/D");
		mynewtree->Branch("p_et",pjet,"pjet[2]/D");
		mynewtree->Branch("p_m",pjm,"pjm[2]/D");

		mynewtree->Branch("p_j1j2px",&pjspx,"pjspx/D");
		mynewtree->Branch("p_j1j2py",&pjspy,"pjspy/D");
		mynewtree->Branch("p_j1j2pz",&pjspz,"pjspz/D");
		mynewtree->Branch("p_j1j2e",&pjse,"pjse/D");
		mynewtree->Branch("p_j1j2eta",&pjseta,"pjseta/D");
		mynewtree->Branch("p_j1j2rap",&pjsrap,"pjsrap/D");
		mynewtree->Branch("p_j1j2phi",&pjsphi,"pjsphi/D");
		mynewtree->Branch("p_j1j2pt",&pjspt,"pjspt/D");
		mynewtree->Branch("p_j1j2et",&pjset,"pjset/D");
		mynewtree->Branch("p_j1j2m",&pjsm,"pjsm/D");

		mynewtree->Branch("p_theta",jpullp,"jpullp[2]/D");
		mynewtree->Branch("p_theta_PID",jcpullp,"jcpullp[2]/D");

		mynewtree->Branch("p_ptfraction",pgfraction_pt,"pgfraction_pt[2]/D");
		mynewtree->Branch("p_dR_match",pjdR,"pjdR[2]/D");

		mynewtree->Branch("wfactor",&wfactor,"wfactor/D");
		mynewtree->Branch("nExtraJets",&nExtraJets,"nExtraJets/I");
		
		TFile *myfile = new TFile(argv[2],"READ");
   	TTree *mytree = (TTree*)myfile->Get("T");
		Long64_t nentries = mytree->GetEntries();
		mytree->SetBranchAddress("bjets_px",jpx);
    mytree->SetBranchAddress("bjets_py",jpy);
    mytree->SetBranchAddress("bjets_pz",jpz);
    mytree->SetBranchAddress("bjets_e",je);
    mytree->SetBranchAddress("pull_thetas",jcpull);
    mytree->SetBranchAddress("cpull_thetas",jpull);
    mytree->SetBranchAddress("cgood_fraction_pt",gfraction_pt);
    mytree->SetBranchAddress("bjets_dR",jdR);

    mytree->SetBranchAddress("p_bjets_px",pjpx);
    mytree->SetBranchAddress("p_bjets_py",pjpy);
    mytree->SetBranchAddress("p_bjets_pz",pjpz);
    mytree->SetBranchAddress("p_bjets_e",pje);
    mytree->SetBranchAddress("p_pull_thetas",jcpullp);
    mytree->SetBranchAddress("p_cpull_thetas",jpullp);
    mytree->SetBranchAddress("p_cgood_fraction_pt",pgfraction_pt);
    mytree->SetBranchAddress("p_bjets_dR",pjdR);

    mytree->SetBranchAddress("treeweight",&weight);
    mytree->SetBranchAddress("UMEPSfactor",&wfactor);
    mytree->SetBranchAddress("nExtraJets",&nExtraJets);

		for (Long64_t i=0;i<nentries; i++) {
      	mytree->GetEntry(i);
      	if(wfactor==0)
      		continue;
      	/*jpx[0]*=1e-3;jpy[0]*=1e-3;jpz[0]*=1e-3;je[0]*=1e-3;
      	jpx[1]*=1e-3;jpy[1]*=1e-3;jpz[1]*=1e-3;je[1]*=1e-3;*/
      	int ibjmatch[2];
      	double jpt1=(jpx[0]*jpx[0] + jpy[0]*jpy[0]);
      	double jpt2=(jpx[1]*jpx[1] + jpy[1]*jpy[1]);
      	if(jpt1>=jpt2){
      		ibjmatch[0]=0;
      		ibjmatch[1]=1;
      	}
      	else{
      		ibjmatch[0]=1;
      		ibjmatch[1]=0;
      	}
      	double jtemp[2];
      	jtemp[0]=jpx[0]; jtemp[1]=jpx[1];
      	jpx[0]=jtemp[ibjmatch[0]];
      	jpx[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpy[0]; jtemp[1]=jpy[1];
      	jpy[0]=jtemp[ibjmatch[0]];
      	jpy[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpz[0]; jtemp[1]=jpz[1];
      	jpz[0]=jtemp[ibjmatch[0]];
      	jpz[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=je[0]; jtemp[1]=je[1];
      	je[0]=jtemp[ibjmatch[0]];
      	je[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpull[0]; jtemp[1]=jpull[1];
      	jpull[0]=jtemp[ibjmatch[0]];
      	jpull[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jcpull[0]; jtemp[1]=jcpull[1];
      	jcpull[0]=jtemp[ibjmatch[0]];
      	jcpull[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jdR[0]; jtemp[1]=jdR[1];
      	jdR[0]=jtemp[ibjmatch[0]];
      	jdR[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=gfraction_pt[0]; jtemp[1]=gfraction_pt[1];
      	gfraction_pt[0]=jtemp[ibjmatch[0]];
      	gfraction_pt[1]=jtemp[ibjmatch[1]];
      	
    	  TLorentzVector j1j2(jpx[0]+jpx[1],jpy[0]+jpy[1],jpz[0]+jpz[1],je[0]+je[1]);
    	  jspx=j1j2.Px();
    	 	jspy=j1j2.Py();
    	 	jspz=j1j2.Pz();
    	  jse=j1j2.E();
    	  jsrap=j1j2.Rapidity();
    	 	jseta=j1j2.Eta();
     	 	jsphi=j1j2.Phi();
     	 	jset=j1j2.Et();
      	jspt=j1j2.Pt();
     	 	jsm=j1j2.M();

     	 	//jpull[0]*=1/acos(-1.0); jpull[1]*=1/acos(-1.0);
				jpull[0]=fabs(jpull[0])/acos(-1.0); jpull[1]=fabs(jpull[1])/acos(-1.0);
				//jcpull[0]*=1/acos(-1.0); jcpull[1]*=1/acos(-1.0);
				jcpull[0]=fabs(jcpull[0])/acos(-1.0); jcpull[1]=fabs(jcpull[1])/acos(-1.0);
      	TLorentzVector j1(jpx[0],jpy[0],jpz[0],je[0]);
      	TLorentzVector j2(jpx[1],jpy[1],jpz[1],je[1]);
      	jphi[0]=j1.Phi();
      	jphi[1]=j2.Phi();
      	jrap[0]=j1.Rapidity();
      	jrap[1]=j2.Rapidity();
      	jeta[0]=j1.Eta();
      	jeta[1]=j2.Eta();
     		jet[0]=j1.Et();
      	jet[1]=j2.Et();
      	jpt[0]=j1.Pt();
      	jpt[1]=j2.Pt();
      	jm[0]=j1.M();
      	jm[1]=j2.M();

      	//Restricted part
      	jtemp[0]=pjpx[0]; jtemp[1]=pjpx[1];
      	pjpx[0]=jtemp[ibjmatch[0]];
      	pjpx[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=pjpy[0]; jtemp[1]=pjpy[1];
      	pjpy[0]=jtemp[ibjmatch[0]];
      	pjpy[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=pjpz[0]; jtemp[1]=pjpz[1];
      	pjpz[0]=jtemp[ibjmatch[0]];
      	pjpz[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=pje[0]; jtemp[1]=pje[1];
      	pje[0]=jtemp[ibjmatch[0]];
      	pje[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jpullp[0]; jtemp[1]=jpullp[1];
      	jpullp[0]=jtemp[ibjmatch[0]];
      	jpullp[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=jcpullp[0]; jtemp[1]=jcpullp[1];
      	jcpullp[0]=jtemp[ibjmatch[0]];
      	jcpullp[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=pjdR[0]; jtemp[1]=pjdR[1];
      	pjdR[0]=jtemp[ibjmatch[0]];
      	pjdR[1]=jtemp[ibjmatch[1]];

      	jtemp[0]=pgfraction_pt[0]; jtemp[1]=pgfraction_pt[1];
      	pgfraction_pt[0]=jtemp[ibjmatch[0]];
      	pgfraction_pt[1]=jtemp[ibjmatch[1]];
      	
    	  TLorentzVector pj1j2(pjpx[0]+pjpx[1],pjpy[0]+pjpy[1],pjpz[0]+pjpz[1],pje[0]+pje[1]);
    	  pjspx=pj1j2.Px();
    	 	pjspy=pj1j2.Py();
    	 	pjspz=pj1j2.Pz();
    	  pjse=pj1j2.E();
    	  pjsrap=pj1j2.Rapidity();
    	 	pjseta=pj1j2.Eta();
     	 	pjsphi=pj1j2.Phi();
     	 	pjset=pj1j2.Et();
      	pjspt=pj1j2.Pt();
     	 	pjsm=pj1j2.M();

     	 	//jpull[0]*=1/acos(-1.0); jpull[1]*=1/acos(-1.0);
				jpullp[0]=fabs(jpullp[0])/acos(-1.0); jpullp[1]=fabs(jpullp[1])/acos(-1.0);
				//jcpull[0]*=1/acos(-1.0); jcpull[1]*=1/acos(-1.0);
				jcpullp[0]=fabs(jcpullp[0])/acos(-1.0); jcpullp[1]=fabs(jcpullp[1])/acos(-1.0);
      	TLorentzVector pj1(pjpx[0],pjpy[0],pjpz[0],pje[0]);
      	TLorentzVector pj2(pjpx[1],pjpy[1],pjpz[1],pje[1]);
      	pjphi[0]=pj1.Phi();
      	pjphi[1]=pj2.Phi();
      	pjrap[0]=pj1.Rapidity();
      	pjrap[1]=pj2.Rapidity();
      	pjeta[0]=pj1.Eta();
      	pjeta[1]=pj2.Eta();
     		pjet[0]=pj1.Et();
      	pjet[1]=pj2.Et();
      	pjpt[0]=pj1.Pt();
      	pjpt[1]=pj2.Pt();
      	pjm[0]=pj1.M();
      	pjm[1]=pj2.M();

      	if(gfraction_pt[0]<0.7 || jdR[0]>0.2 || pgfraction_pt[0]<0.7 || pjdR[0]>0.2)
      		continue;

      	mynewtree->Fill();
      }
     	myfile->Close();
	}
	newfile->cd();
	mynewtree->Write();
	newfile->Close();
}