#include <TStyle.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TH2F.h>
#include <TTree.h>
#include <math.h>
#include <iostream>

using namespace std;

int main(int argc,char* argv[]){
   //TString outfilename = argv[1];
   TFile *myfile = new TFile("myhists.root","UPDATE");
	//TFile *newwfile = new TFile("","UPDATE");
	bool res;
	TString hname;

	hname= TString(argv[1]) + "_pull_diff_j1";
    TH1D h1(hname,"J1 Pull Diff",20,-1.0,1.0);
	hname = TString(argv[2]) + "_pull_j1;1";
	TH1D *h1j1=(TH1D*)myfile->Get(hname);
	hname = TString(argv[3]) + "_pull_j1;1";
	TH1D *h2j1=(TH1D*)myfile->Get(hname);
	h1j1->Sumw2(); h2j1->Sumw2();
	res = h1.Add(h1j1,h2j1,1.0,-1.0);
	if(!res) std::cout<<"Failed for *_pull_j1"<<endl;

	hname = TString(argv[1]) + "_pull_diff_j2";
    TH1D h2(hname,"J2 Pull Diff",20,-1.0,1.0);
	hname = TString(argv[2]) + "_pull_j2;1";
	TH1D *h1j2=(TH1D*)myfile->Get(hname);
	hname = TString(argv[3]) + "_pull_j2;1";
	TH1D *h2j2=(TH1D*)myfile->Get(hname);
	h1j2->Sumw2(); h2j2->Sumw2();
	res = h2.Add(h1j2,h2j2,1.0,-1.0);
	if(!res) std::cout<<"Failed for *_pull_j2"<<endl;
	

	hname = TString(argv[1]) + "_pullr_diff_j1";
    TH1D hr1(hname,"J1 Pull Diff: 2.5>eta>4.5",20,-1.0,1.0);
	hname = TString(argv[2]) + "_pull_j1;1";
	TH1D *h1rj1=(TH1D*)myfile->Get(hname);
	hname = TString(argv[3]) + "_pullr_j1;1";
	TH1D *h2rj1=(TH1D*)myfile->Get(hname);
	h1rj1->Sumw2(); h2rj1->Sumw2();
	res = hr1.Add(h1rj1,h2rj1,1.0,-1.0);
	if(!res) std::cout<<"Failed for *_pullr_j1"<<endl;


	hname = TString(argv[1]) + "_pullr_diff_j2";	
    TH1D hr2(hname,"J2 Pull Diff: 2.5>eta>4.5",20,-1.0,1.0);
	hname = TString(argv[2]) + "_pull_j2;1";
	TH1D *h1rj2=(TH1D*)myfile->Get(hname);
	hname = TString(argv[3]) + "_pullr_j2;1";
	TH1D *h2rj2=(TH1D*)myfile->Get(hname);
	h1rj2->Sumw2(); h2rj2->Sumw2();
	res = hr2.Add(h1rj2,h2rj2,1.0,-1.0);
	if(!res) std::cout<<"Failed for *_pullr_j2"<<endl;

   h1.Write();
   h2.Write();
   hr1.Write();
   hr2.Write();
   myfile->Close();
   //delete oldfile;
}
