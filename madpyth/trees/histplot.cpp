#include <TStyle.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVectorD.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TAxis.h>
#include <TTree.h>
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc,char* argv[]){
	//arg1 type of tree(s): {D=Data,MCU=MCUnrestricted,MCR=MCRestricted}
	//arg2 oldtreefilepath(s)
   //TString newfilename;
   //newfilename=TString(argv[1])+".root";
	TFile *newfile = new TFile("myhists.root","update");
  double jpx[2],jpy[2],jpz[2],je[2],jeta[2],jrap[2],jphi[2],jpt[2],jet[2],jm[2];
  double jspx,jspy,jspz,jse,jseta,jsrap,jsphi,jspt,jset,jsm,weight;
  double jpull[2];

  TString mytname;
	mytname = TString(argv[1])+"T1";
  TFile *myfile = new TFile(argv[2],"READ");
  TTree *mytree = (TTree*)myfile->Get(mytname);
	Long64_t nentries = mytree->GetEntries();

  mytree->SetBranchAddress("weight",&weight);
	mytree->SetBranchAddress("px",jpx);
	mytree->SetBranchAddress("py",jpy);
	mytree->SetBranchAddress("pz",jpz);
	mytree->SetBranchAddress("e",je);
	mytree->SetBranchAddress("eta",jeta);
	mytree->SetBranchAddress("rap",jrap);
	mytree->SetBranchAddress("phi",jphi);
	mytree->SetBranchAddress("pt",jpt);
	mytree->SetBranchAddress("et",jet);
	mytree->SetBranchAddress("m",jm);

	mytree->SetBranchAddress("j1j2px",&jspx);
	mytree->SetBranchAddress("j1j2py",&jspy);
	mytree->SetBranchAddress("j1j2pz",&jspz);
	mytree->SetBranchAddress("j1j2e",&jse);
	mytree->SetBranchAddress("j1j2eta",&jseta);
	mytree->SetBranchAddress("j1j2rap",&jsrap);
	mytree->SetBranchAddress("j1j2phi",&jsphi);
	mytree->SetBranchAddress("j1j2pt",&jspt);
	mytree->SetBranchAddress("j1j2et",&jset);
	mytree->SetBranchAddress("j1j2m",&jsm);

	mytree->SetBranchAddress("theta",jpull);

	if(TString(argv[1]).Contains("D")){
		UInt_t npv;
   	mytree->SetBranchAddress("nPVs",&npv);

   	//Hists to plot

   	TH1D h_theta("Theta:"+TString(argv[1]),"Theta:"TString(argv[1]),10,0,1.0);
   	TH1D h_thetaR("ThetaR:"+TString(argv[1])+" (2.8>eta>4)","ThetaR:"+TString(argv[1])+" (2.8>eta>4)",10,0,1.0);
   	TH1D h_eta("Eta:"+TString(argv[1]),"Eta:"+TString(argv[1]),20,2,4.5);
   	TH2D h_thetaveta("(Theta v Eta):"+TString(argv[1]),"(Theta v Eta):"+TString(argv[1]),20,2,4.5,10,0,1.0);

   	for(Long64_t i=0;i<nentries;++i){
   		mytree->GetEntry(i);
   		if(npv!=1)
   			continue;
   		h_theta.Fill(jpull[0]);
   		h_eta.Fill(jeta[0]);
   		if(jeta[0]>2.8 && jeta[0]<4)
   			h_thetaR.Fill(jpull[0]);
   	}
   	for(Long64_t i=0;i<nentries;++i){
   		mytree->GetEntry(i);
   		if(npv!=1)
   			continue;
   		double etaweight;
   		etaweight = h_eta.GetBinContent(h_eta.FindBin(jeta[0]));
   		h_thetaveta.Fill(jeta[0],jpull[0],1/etaweight);
   	}
   	myfile->Close();
   	newfile->cd();
   	h_theta.Sumw2();
   	h_thetaR.Sumw2();
   	h_eta.Sumw2();

   	h_theta.Write();
   	h_thetaR.Write();
   	h_eta.Write();
   	h_thetaveta.Write();
	}
	else if(TString(argv[1]).Contains("MCU")){
		double gfraction_pt[2],jdR[2],jcpull[2],wfactor;
		int nExtraJets;

		mytree->SetBranchAddress("theta_PID",jcpull);
		mytree->SetBranchAddress("dR_match",jdR);
		mytree->SetBranchAddress("nExtraJets",&nExtraJets);
		mytree->SetBranchAddress("ptfraction",gfraction_pt);
		mytree->SetBranchAddress("wfactor",&wfactor);


		TH1D h_theta("Theta:MCU","Theta:MCU",10,0,1.0);
		TH1D h_thetaPID("Theta_PID:MCU","Theta_PID:MCU",10,0,1.0);
   	//TH1D h_thetaR("ThetaR:MCU (2.8>eta>4)","ThetaR:MCU (2.8>eta>4)",10,0,1.0);
   	//TH1D h_thetaRPID("ThetaR_PID:MCU (2.8>eta>4)","ThetaR_PID:MCU (2.8>eta>4)",10,0,1.0);
   	TH1D h_eta("Eta:MCU","Eta:MCU",20,2,4.5);
   	TH1D h_etaUnweighted("Eta:MCU (Unweighted)","Eta:MCU (Unweighted)",20,2,4.5);
   	TH2D h_thetaveta("(Theta v Eta):MCU","(Theta v Eta):MCU",20,2,4.5,10,0,1.0);
   	//TH1D h_etaPID("Eta_PID:MCU","Eta_PID:MCU",20,2,4.5);
   	TH2D h_thetavetaPID("(Theta v Eta)_PID:MCU","(Theta v Eta)_PID:MCU",20,2,4.5,10,0,1.0);

   	for(Long64_t i=0;i<nentries;++i){
   		mytree->GetEntry(i);

   		h_theta.Fill(jpull[0],weight*wfactor);
   		h_eta.Fill(jeta[0],weight*wfactor);
   		h_etaUnweighted.Fill(jeta[0]);
   		h_thetaPID.Fill(jcpull[0],weight*wfactor);
   	
   		/*if(jeta[0]>2.8 && jeta[0]<4)
   			h_thetaR.Fill(jpull[0],weight*wfactor);
   			h_thetaRPID.Fill(jcpull[0],weight*wfactor);*/
   		}
   	for(Long64_t i=0;i<nentries;++i){
   		mytree->GetEntry(i);
   		
   		double etaweight;
   		etaweight = h_etaUnweighted.GetBinContent(h_etaUnweighted.FindBin(jeta[0]));
   		h_thetaveta.Fill(jeta[0],jpull[0],weight*wfactor/etaweight);
   		h_thetavetaPID.Fill(jeta[0],jcpull[0],weight*wfactor/etaweight);
   	}

   	myfile->Close();
   	newfile->cd();
   	h_theta.Sumw2();
   	//h_thetaR.Sumw2();
   	h_thetaPID.Sumw2();
   	//h_thetaRPID.Sumw2();
   	h_eta.Sumw2();

   	h_theta.Write();
   	//h_thetaR.Write();
   	h_thetaPID.Write();
   	//h_thetaRPID.Write();
   	h_eta.Write();
   	h_thetaveta.Write();
   	h_thetavetaPID.Write();

	}
	else if(TString(argv[1]).Contains("MCR")){
		double gfraction_pt[2],jdR[2],jcpull[2],wfactor;
		int nExtraJets;

		mytree->SetBranchAddress("theta_PID",jcpull);
		mytree->SetBranchAddress("dR_match",jdR);
		mytree->SetBranchAddress("nExtraJets",&nExtraJets);
		mytree->SetBranchAddress("ptfraction",gfraction_pt);
		mytree->SetBranchAddress("wfactor",&wfactor);

		double pjpx[2],pjpy[2],pjpz[2],pje[2],pjeta[2],pjrap[2],pjphi[2],pjet[2],pjpt[2],pjm[2];
		double pjspx,pjspy,pjspz,pjse,pjseta,pjsrap,pjsphi,pjset,pjspt,pjsm;
		double pjpull[2],pjcpull[2],pulldiff,pgfraction_pt[2],pjdR[2];

		mytree->SetBranchAddress("p_px",pjpx);
		mytree->SetBranchAddress("p_py",pjpy);
		mytree->SetBranchAddress("p_pz",pjpz);
		mytree->SetBranchAddress("p_e",pje);
		mytree->SetBranchAddress("p_eta",pjeta);
		mytree->SetBranchAddress("p_rap",pjrap);
		mytree->SetBranchAddress("p_phi",pjphi);
		mytree->SetBranchAddress("p_pt",pjpt);
		mytree->SetBranchAddress("p_et",pjet);
		mytree->SetBranchAddress("p_m",pjm);

		mytree->SetBranchAddress("p_j1j2px",&pjspx);
		mytree->SetBranchAddress("p_j1j2py",&pjspy);
		mytree->SetBranchAddress("p_j1j2pz",&pjspz);
		mytree->SetBranchAddress("p_j1j2e",&pjse);
		mytree->SetBranchAddress("p_j1j2eta",&pjseta);
		mytree->SetBranchAddress("p_j1j2rap",&pjsrap);
		mytree->SetBranchAddress("p_j1j2phi",&pjsphi);
		mytree->SetBranchAddress("p_j1j2pt",&pjspt);
		mytree->SetBranchAddress("p_j1j2et",&pjset);
		mytree->SetBranchAddress("p_j1j2m",&pjsm);

		mytree->SetBranchAddress("p_theta",pjpull);
		mytree->SetBranchAddress("p_theta_PID",pjcpull);

		mytree->SetBranchAddress("p_ptfraction",pgfraction_pt);
		mytree->SetBranchAddress("p_dR_match",pjdR);

		TH1D h_theta("Theta:MCR","Theta:MCR",10,0,1.0);
		TH1D h_thetaPID("Theta_PID:MCR","Theta_PID:MCR",10,0,1.0);
		TH1D h_thetaRest("Theta_Rest:MCR","Theta_Rest:MCR",10,0,1.0);
   	TH1D h_thetaR("ThetaR:MCR (2.8>eta>4)","ThetaR:MCR (2.8>eta>4)",10,0,1.0);
   	TH1D h_thetaRRest("ThetaR_Rest:MCR (2.8>eta>4)","ThetaR_Rest:MCR (2.8>eta>4)",10,0,1.0);
   	TH1D h_thetaRPID("ThetaR_PID:MCR (2.8>eta>4)","ThetaR_PID:MCR (2.8>eta>4)",10,0,1.0);
   	TH1D h_eta("Eta:MCR","Eta:MCR",20,2,4.5);
   	TH1D h_etaUnweighted("Eta:MCR (Unweighted)","Eta:MCR (Unweighted)",20,2,4.5);
   	TH1D h_etaUnweightedRest("Eta_Rest:MCR (Unweighted)","Eta_Rest:MCR (Unweighted)",20,2,4.5);
   	TH2D h_thetaveta("(Theta v Eta):MCR","(Theta v Eta):MCR",20,2,4.5,10,0,1.0);
   	//TH1D h_etaPID("Eta_PID:MCU","Eta_PID:MCU",20,2,4.5);
   	TH2D h_thetavetaPID("(Theta v Eta)_PID:MCR","(Theta v Eta)_PID:MCR",20,2,4.5,10,0,1.0);
   	TH2D h_thetavetaRest("(Theta v Eta)_Rest:MCR","(Theta v Eta)_Rest:MCR",20,2,4.5,10,0,1.0);

   	for(Long64_t i=0;i<nentries;++i){
   		mytree->GetEntry(i);

   		h_theta.Fill(jpull[0],weight*wfactor);
   		h_thetaPID.Fill(jcpull[0],weight*wfactor);
   		h_thetaRest.Fill(pjpull[0],weight*wfactor);
   		h_eta.Fill(jeta[0],weight*wfactor);
   		h_etaUnweighted.Fill(jeta[0]);
   		h_etaUnweightedRest.Fill(pjeta[0]);
   	
   		if(jeta[0]>2.8 && jeta[0]<4){
   			h_thetaR.Fill(jpull[0],weight*wfactor);
   			h_thetaRPID.Fill(jcpull[0],weight*wfactor);
   			h_thetaRRest.Fill(pjpull[0],weight*wfactor);
   		}
   	}
   	for(Long64_t i=0;i<nentries;++i){
   		mytree->GetEntry(i);
   		
   		double etaweight,petaweight;
   		petaweight = h_etaUnweightedRest.GetBinContent(h_etaUnweightedRest.FindBin(pjeta[0]));
   		etaweight = h_etaUnweighted.GetBinContent(h_etaUnweighted.FindBin(jeta[0]));
   		h_thetaveta.Fill(jeta[0],jpull[0],weight*wfactor/etaweight);
   		h_thetavetaPID.Fill(jeta[0],jcpull[0],weight*wfactor/etaweight);
   		h_thetavetaRest.Fill(pjeta[0],pjpull[0],weight*wfactor/petaweight);
   	}

   	myfile->Close();
   	newfile->cd();
   	h_theta.Sumw2();
   	h_thetaR.Sumw2();
   	h_thetaPID.Sumw2();
   	h_thetaRPID.Sumw2();
   	h_thetaRest.Sumw2();
   	h_thetaRRest.Sumw2();
   	h_eta.Sumw2();

   	h_theta.Write();
   	h_thetaR.Write();
   	h_thetaPID.Write();
   	h_thetaRPID.Write();
   	h_eta.Write();
   	h_thetaveta.Write();
   	h_thetavetaPID.Write();
   	h_thetavetaRest.Write();
	}

	newfile->Close();

}