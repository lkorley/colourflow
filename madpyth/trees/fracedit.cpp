#include <TStyle.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TString.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TVectorD.h>
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TAxis.h>
#include <TTree.h>
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc,char* argv[]){

   TString hname;
	/*hname = TString(argv[1]) + "_pullm2";
   TH1D h_pullm2(hname,"Mean Sqrt Pull",100,-1.0,1.0);
hname = TString(argv[1]) + "_|pull|_j2";
   TH1D h_pull2_j2(hname,"J2 |Pull|",100,-1.0,1.0);

	hname = TString(argv[1]) + "_|pull|_j1";
   TH1D h_pull2_j1(hname,"J1 |Pull|",100,-1.0,1.0);*/

	hname = TString(argv[1]) + "_eta:pull1";
   TH2D h_etapull1(hname,"eta:pull1",20,2,4,10,0,1.0);

   hname = TString(argv[1]) + "_eta:pull2";
   TH2D h_etapull2(hname,"eta:pull2",20,2,4,10,0,1.0);

   hname = TString(argv[1]) + "_eta1";
   TH1D h_eta1(hname,"eta1",20,2,4);

   hname = TString(argv[1]) + "_eta2";
   TH1D h_eta2(hname,"eta2",20,2,4);

   hname = TString(argv[1]) + "_pull_j1";
   TH1D h_pull_j1(hname,"J1 Pull",10,0,1.0);

	hname = TString(argv[1]) + "_pullr_j1";
   TH1D h_pullr_j1(hname,"J1 Pull: 2.8>eta>4",10,0,1.0);

   hname = TString(argv[1]) + "_pull_j2";
   TH1D h_pull_j2(hname,"J2 Pull",10,0,1.0);

   hname = TString(argv[1]) + "_pullr_j2";	
   TH1D h_pullr_j2(hname,"J2 Pull: 2.8>eta>4",10,0,1.0);


   TFile *newfile = new TFile("myhists.root","update");
   double jpx[2],jpy[2],jpz[2],je[2],jeta[2],jrap[2],jphi[2],jpt[2],jet[2],jm[2];
   double jspx,jspy,jspz,jse,jseta,jsrap,jsphi,jspt,jset,jsm;
    double jpull[2];


	TString mytname;
	mytname = TString(argv[1])+"T1";
	TTree *mynewtree = new TTree(mytname,mytname);
	mynewtree->Branch("px",jpx,"jpx[2]/D");
	mynewtree->Branch("py",jpy,"jpy[2]/D");
	mynewtree->Branch("pz",jpz,"jpz[2]/D");
	mynewtree->Branch("e",je,"je[2]/D");
	mynewtree->Branch("eta",jeta,"jeta[2]/D");
	mynewtree->Branch("rap",jrap,"jrap[2]/D");
	mynewtree->Branch("phi",jphi,"jphi[2]/D");
	mynewtree->Branch("pt",jpt,"jpt[2]/D");
	mynewtree->Branch("et",jet,"jet[2]/D");
	mynewtree->Branch("m",jm,"jm[2]/D");


	mynewtree->Branch("j1j2px",&jspx,"jspx/D");
	mynewtree->Branch("j1j2py",&jspy,"jspy/D");
	mynewtree->Branch("j1j2pz",&jspz,"jspz/D");
	mynewtree->Branch("j1j2e",&jse,"jse/D");
	mynewtree->Branch("j1j2eta",&jseta,"jseta/D");
	mynewtree->Branch("j1j2rap",&jsrap,"jsrap/D");
	mynewtree->Branch("j1j2phi",&jsphi,"jsphi/D");
	mynewtree->Branch("j1j2pt",&jspt,"jspt/D");
	mynewtree->Branch("j1j2et",&jset,"jset/D");
	mynewtree->Branch("j1j2m",&jsm,"jsm/D");

	mynewtree->Branch("pull",jpull,"jpull[2]/D");
	


if(TString(argv[1]).Contains("D")){
	 UInt_t npv;
   mynewtree->Branch("nPVs",&npv,"npv/i");

 	TFile *myfile = new TFile(argv[2],"READ");
 //for(int j=3;j<=argc;++j){
		//TString treename;
		//treename = TString(argv[j]);
		TTree *mytree = (TTree*)myfile->Get("T");
   Long64_t nentries = mytree->GetEntries();
  
   mytree->SetBranchAddress("nPVs",&npv);
   mytree->SetBranchAddress("j1px",&(jpx[0]));
   mytree->SetBranchAddress("j2px",&(jpx[1]));
   mytree->SetBranchAddress("j1py",&(jpy[0]));
   mytree->SetBranchAddress("j2py",&(jpy[1]));
   mytree->SetBranchAddress("j1pz",&(jpz[0]));
   mytree->SetBranchAddress("j2pz",&(jpz[1]));
   mytree->SetBranchAddress("j1e",&(je[0]));
   mytree->SetBranchAddress("j2e",&(je[1]));

   mytree->SetBranchAddress("j1theta",&(jpull[0]));
   mytree->SetBranchAddress("j2theta",&(jpull[1]));
   for (Long64_t i=0;i<nentries; i++) {
      mytree->GetEntry(i);
      jpx[0]*=1e-3;jpy[0]*=1e-3;jpz[0]*=1e-3;je[0]*=1e-3;
      jpx[1]*=1e-3;jpy[1]*=1e-3;jpz[1]*=1e-3;je[1]*=1e-3;
      jpull[0]*=1/acos(-1.0); jpull[1]*=1/acos(-1.0);
	jpull[0]=fabs(jpull[0]); jpull[1]=fabs(jpull[1]);
      TLorentzVector j1j2(jpx[0]+jpx[1],jpy[0]+jpy[1],jpz[0]+jpz[1],je[0]+je[1]);
      jspx=j1j2.Px();
      jspy=j1j2.Py();
      jspz=j1j2.Pz();
      jse=j1j2.E();
      jsrap=j1j2.Rapidity();
      jseta=j1j2.Eta();
      jsphi=j1j2.Phi();
      jset=j1j2.Et();
      jspt=j1j2.Pt();
      jsm=j1j2.M();

      TLorentzVector j1(jpx[0],jpy[0],jpz[0],je[0]);
      TLorentzVector j2(jpx[1],jpy[1],jpz[1],je[1]);
      jphi[0]=j1.Phi();
      jphi[1]=j2.Phi();
      jrap[0]=j1.Rapidity();
      jrap[1]=j2.Rapidity();
      jeta[0]=j1.Eta();
      jeta[1]=j2.Eta();
      jet[0]=j1.Et();
      jet[1]=j2.Et();
      jpt[0]=j1.Pt();
      jpt[1]=j2.Pt();
      jm[0]=j1.M();
      jm[1]=j2.M();

      h_pull_j1.Fill(jpull[0]);
      h_eta1.Fill(jeta[0]);
      h_pull_j2.Fill(jpull[1]);
      h_eta2.Fill(jeta[1]);
      if(jeta[0]>2.8&&jeta[0]<4){
      	h_pullr_j1.Fill(jpull[0]);
      }
      if(jeta[1]>2.8&&jeta[1]<4){
      	h_pullr_j2.Fill(jpull[1]);
      }
      mynewtree->Fill();
   }
 myfile->Close();

 	//TString treename;
	//treename = TString(argv[j]);
	//TTree *mytree = (TTree*)myfile->Get(treename);
   nentries = mynewtree->GetEntries();
   //UInt_t npv;


   mynewtree->SetBranchAddress("pull",jpull);
   mynewtree->SetBranchAddress("eta",jeta);

   for (Long64_t i=0;i<nentries; i++) {
      mynewtree->GetEntry(i);
      //jpull[0]=fabs(jpull[0]); jpull[1]=fabs(jpull[1]);
      
      double etaweight;
      etaweight = h_eta1.GetBinContent(h_eta1.FindBin(jeta[0]));
      h_etapull1.Fill(jeta[0],jpull[0],1/etaweight);
      etaweight = h_eta2.GetBinContent(h_eta2.FindBin(jeta[1]));
      h_etapull2.Fill(jeta[1],jpull[1],1/etaweight);

    }
 
    //mytree->Close();
    
    newfile->cd();
    mynewtree->Write();
   h_pull_j1.Sumw2();
   h_pull_j2.Sumw2();

   h_pullr_j1.Sumw2();
   h_pullr_j2.Sumw2();

   h_etapull1.Sumw2();
   h_etapull2.Sumw2();

   h_pull_j1.Write();
   h_pull_j2.Write();

   h_pullr_j1.Write();
   h_pullr_j2.Write();

   h_etapull1.Write();
   h_etapull2.Write();

}
else{

   hname = TString(argv[1]) + "_p_eta:pull1";
   TH2D h_petapull1(hname,"p_eta:pull1",20,2,4,10,0,1.0);

   hname = TString(argv[1]) + "_p_eta:pull2";
   TH2D h_petapull2(hname,"p_eta:pull2",20,2,4,10,0,1.0);

   hname = TString(argv[1]) + "_eta:diff1";
   TH2D h_etadiff1(hname,"eta:diff1",20,2,4,20,-1.0,1.0);

   hname = TString(argv[1]) + "_eta:diff2";
   TH2D h_etadiff2(hname,"eta:diff2",20,2,4,20,-1.0,1.0);

	
   hname = TString(argv[1]) + "_etap1";
   TH1D h_etap1(hname,"eta1",20,2,4);
   
   hname = TString(argv[1]) + "_etap2";
   TH1D h_etap2(hname,"eta2",20,2,4);

	hname = TString(argv[1]) + "_pulldiff1";
   TH2D h_pulld1(hname,"Pulldiff1",10,0,1.0,40,-1.0,1.0);

   hname = TString(argv[1]) + "_pulldiff2";
   TH2D h_pulld2(hname,"Pulldiff2",10,0,1.0,40,-1.0,1.0);

   hname = TString(argv[1]) + "_pulldiff1g";
   TH2D h_pulld1g(hname,"Pulldiff1:Diff>0.1",10,0,1.0,40,-1.0,1.0);

   hname = TString(argv[1]) + "_pulldiff2g";
   TH2D h_pulld2g(hname,"Pulldiff2:diff>0.1",10,0,1.0,40,-1.0,1.0);
	
	hname = TString(argv[1]) + "_p_pull_j1";
   TH1D h_pullp_j1(hname,"J1 Pull",10,0,1.0);
	hname = TString(argv[1]) + "_p_pullr_j1";
   TH1D h_pullpr_j1(hname,"J1 Pull: 2.8>eta>4",10,0,1.0);
	
	hname = TString(argv[1]) + "_p_pull_j2";
   TH1D h_pullp_j2(hname,"J2 Pull",10,0,1.0);
	hname = TString(argv[1]) + "_p_pullr_j2";	
   TH1D h_pullpr_j2(hname,"J2 Pull: 2.8>eta>4",10,0,1.0);

   hname = TString(argv[1]) + "_p_pulldiffg_j1";
   TH1D h_pullg_j1(hname,"J1 Pull: diff>0.1",10,0,1.0);

   hname = TString(argv[1]) + "_p_pulldiffs_j1";
   TH1D h_pulls_j1(hname,"J1 Pull: diff<0.1",10,0,1.0);

   hname = TString(argv[1]) + "_p_pulldiffg_j2";
   TH1D h_pullg_j2(hname,"J2 Pull: diff>0.1",10,0,1.0);

   hname = TString(argv[1]) + "_p_pulldiffs_j2";
   TH1D h_pulls_j2(hname,"J2 Pull: diff<0.1",10,0,1.0);
	

double pjpx[2],pjpy[2],pjpz[2],pje[2],pjeta[2],pjrap[2],pjphi[2],pjet[2],pjpt[2],pjm[2];
double pjspx,pjspy,pjspz,pjse,pjseta,pjsrap,pjsphi,pjset,pjspt,pjsm;
double jpullp[2],pulldiff,gfraction_pt[2],weight;

    mynewtree->Branch("p_px",pjpx,"pjpx[2]/D");
	mynewtree->Branch("p_py",pjpy,"pjpy[2]/D");
	mynewtree->Branch("p_pz",pjpz,"pjpz[2]/D");
	mynewtree->Branch("p_e",pje,"pje[2]/D");
	mynewtree->Branch("p_eta",pjeta,"pjeta[2]/D");
	mynewtree->Branch("p_rap",pjrap,"pjrap[2]/D");
	mynewtree->Branch("p_phi",pjphi,"pjphi[2]/D");
	mynewtree->Branch("p_pt",pjpt,"pjpt[2]/D");
	mynewtree->Branch("p_et",pjet,"pjet[2]/D");
	mynewtree->Branch("p_m",pjm,"pjm[2]/D");

	mynewtree->Branch("p_j1j2px",&pjspx,"pjspx/D");
	mynewtree->Branch("p_j1j2py",&pjspy,"pjspy/D");
	mynewtree->Branch("p_j1j2pz",&pjspz,"pjspz/D");
	mynewtree->Branch("p_j1j2e",&pjse,"pjse/D");
	mynewtree->Branch("p_j1j2eta",&pjseta,"pjseta/D");
	mynewtree->Branch("p_j1j2rap",&pjsrap,"pjsrap/D");
	mynewtree->Branch("p_j1j2phi",&pjsphi,"pjsphi/D");
	mynewtree->Branch("p_j1j2pt",&pjspt,"pjspt/D");
	mynewtree->Branch("p_j1j2et",&pjset,"pjset/D");
	mynewtree->Branch("p_j1j2m",&pjsm,"pjsm/D");

    mynewtree->Branch("ptfraction",gfraction_pt,"gfraction_pt[2]/D");
	mynewtree->Branch("pullp",jpullp,"jpullp[2]/D");
	mynewtree->Branch("pulldiff",&pulldiff,"pulldiff/D");

	mynewtree->Branch("weight",&weight,"weight/D");

TFile *myfile = new TFile(argv[2],"READ");

		TTree *mytree = (TTree*)myfile->Get("T");
   Long64_t nentries = mytree->GetEntries();
   //Event *event   = 0;
   //double pull_mags[2],pull_angles[2],pull_raps[2],pull_phis[2],pull_thetas[2],p_pull_thetas[2],p_bjets_eta[2];
   //double bjets_eta[2],bjets_phi[2],bjets_rap[2],bjets_pt[2],bjets_eta_mu[2];
   double bjets_px[2],bjets_py[2],bjets_pz[2],bjets_e[2],j1j2dphi;
   double p_bjets_px[2],p_bjets_py[2],p_bjets_pz[2],p_bjets_e[2],p_j1j2dphi;
   //double bjets_spread_eta[2],bjets_phi_recreatemu[2],bjets_pt_mu[2],bjets_M,good_fraction_pt[2];
   double pull_thetas[2],p_pull_thetas[2],bjets_eta[2],good_fraction_pt[2],treeweight;
   int good_constituents[2];
   int nbjets=2,ngoodevents=0;

   mytree->SetBranchAddress("nbjets",&nbjets);
   mytree->SetBranchAddress("weight",&treeweight);
  // mytree->SetBranchAddress("bjets_eta",bjets_eta);
   mytree->SetBranchAddress("p_bjets_px",p_bjets_px);
   mytree->SetBranchAddress("p_bjets_py",p_bjets_py);
   mytree->SetBranchAddress("p_bjets_pz",p_bjets_pz);
   mytree->SetBranchAddress("p_bjets_e",p_bjets_e);
   mytree->SetBranchAddress("p_j1j2dphi",&p_j1j2dphi);

   mytree->SetBranchAddress("p_bjets_eta",bjets_eta);

   mytree->SetBranchAddress("bjets_px",bjets_px);
   mytree->SetBranchAddress("bjets_py",bjets_py);
   mytree->SetBranchAddress("bjets_pz",bjets_pz);
   mytree->SetBranchAddress("bjets_e",bjets_e);
   mytree->SetBranchAddress("j1j2dphi",&j1j2dphi);

   mytree->SetBranchAddress("pull_thetas",pull_thetas);
   mytree->SetBranchAddress("p_pull_thetas",p_pull_thetas);
   mytree->SetBranchAddress("cgood_constituents",good_constituents);
   mytree->SetBranchAddress("cgood_fraction_pt",good_fraction_pt);


	//mytree->SetBranchAddress("p_bjets_eta",p_bjets_eta);
   //mytree->SetBranchAddress("bjets_pt",bjets_pt);
   /*oldtree->SetBranchAddress("bjets_eta_mu",bjets_eta_mu);
   oldtree->SetBranchAddress("bjets_phi_mu",bjets_phi_mu);
   oldtree->SetBranchAddress("bjets_phi",bjets_phi);
   oldtree->SetBranchAddress("bjets_rap",bjets_rap);
   oldtree->SetBranchAddress("bjets_pt_mu",bjets_pt_mu);
   
   oldtree->SetBranchAddress("pull_mags",pull_mags);
   oldtree->SetBranchAddress("utfilename = argv[1];
   TFile *oldfile = new TFile(outfilename,"READ");
   TTree *oldtree = (TTree*)oldfile->Get("T");
   Long64_t nentries = oldtree->GetEntries();
   //Event *event   = 0;
   double pull_mags[2],pull_angles[2],pull_raps[2],pull_phis[2],pull_thetas[2];
   double bjets_eta[2],bjets_phi[2],bjets_rap[2],bjets_pt[2],bjets_eta_mu[2];
   double bjets_spread_etpull_angles",pull_angles);
   oldtree->SetBranchAddress("pull_raps",pull_raps);
   oldtree->SetBranchAddress("pull_phis",pull_phis);*/
   
   /*oldtree->SetBranchAddress("bjets_M",&bjets_M);
   oldtree->SetBranchAddress("good_fraction_pt",good_fraction_pt);*/

   //Create a new file + a clone of old tree in new file
   /*TString newfilename = "new/";
   newfilename.operator+=(outfilename);
   TFile *newfile = new TFile(newfilename,"recreate");
   TTree *newtree = new TTree("T","T");
   */

TList* info=mytree->GetUserInfo();
TVectorD* xsec=(TVectorD*)info->First();
TVectorD* nAcc=(TVectorD*)info->First();
double xsecweight=1e9*(*xsec)[0]/(*nAcc)[0];
//double xsecerr = (*xsec)[1]/(*xsec)[0];

   for (Long64_t i=0;i<nentries; i++) {
      mytree->GetEntry(i);
	weight=treeweight;
			if(good_constituents[0]<=2 || good_constituents[1]<=2) continue;
			/*double m2pull = pull_thetas[0]*pull_thetas[0] + pull_thetas[1]*pull_thetas[1];
			m2pull = sqrt(m2pull)/acos(-1.0);
			h_pullm2.Fill(m2pull);
			h_pull2d.Fill(pull_thetas[0]/acos(-1.0),pull_thetas[1]/acos(-1.0));*/
		TLorentzVector jdiff1(bjets_px[0],bjets_py[0],bjets_pz[0],bjets_e[0]);
		TLorentzVector jdiff2(bjets_px[1],bjets_py[1],bjets_pz[1],bjets_e[1]);
		TLorentzVector j1j2=jdiff1.operator+(jdiff2);
		TLorentzVector jdiffp1(p_bjets_px[0],p_bjets_py[0],p_bjets_pz[0],p_bjets_e[0]);
		TLorentzVector jdiffp2(p_bjets_px[1],p_bjets_py[1],p_bjets_pz[1],p_bjets_e[1]);
		TLorentzVector j1j2p=jdiffp1.operator+(jdiffp2);
		jspx = j1j2.Px(); pjspx = j1j2p.Px();
		jspy = j1j2.Py(); pjspy = j1j2p.Py();
		jspz = j1j2.Pz(); pjspz = j1j2p.Pz();
		jse = j1j2.E(); pjse = j1j2p.E();
		jseta = j1j2.Eta(); pjseta = j1j2p.Eta();
		jsrap = j1j2.Rapidity(); pjsrap = j1j2p.Rapidity();
		jsphi = j1j2.Phi(); pjsphi = j1j2p.Phi();
		jset = j1j2.Et(); pjset = j1j2p.Et();
		jspt = j1j2.Pt(); pjspt = j1j2p.Pt();
		jsm = j1j2.M(); pjsm = j1j2p.M();

		jphi[0]=jdiff1.Phi(); jphi[1]=jdiff2.Phi();
		jpt[0]=jdiff1.Pt(); jpt[1]=jdiff2.Pt();
      	jet[0]=jdiff1.Et(); jet[1]=jdiff2.Et();
      	jeta[0]=jdiff1.Eta(); jeta[1]=jdiff2.Eta();
      	jrap[0]=jdiff1.Rapidity(); jrap[1]=jdiff2.Rapidity();
      	jm[0]=jdiff1.M(); jm[1]=jdiff2.M();

      	pjphi[0]=jdiffp1.Phi(); pjphi[1]=jdiffp2.Phi();
      	pjpt[0]=jdiffp1.Pt(); pjpt[1]=jdiffp2.Pt();
      	pjet[0]=jdiffp1.Et(); pjet[1]=jdiffp2.Et();
      	pjeta[0]=jdiff1.Eta(); pjeta[1]=jdiff2.Eta();
      	pjrap[0]=jdiff1.Rapidity(); pjrap[1]=jdiff2.Rapidity();
      	pjm[0]=jdiffp1.M(); pjm[1]=jdiffp2.M();


		//h_etapull1.Fill(jdiff.Eta(),pull);
      for(int ibj=0;ibj<nbjets;++ibj){
      			jpx[ibj]=bjets_px[ibj];
      			jpy[ibj]=bjets_py[ibj];
      			jpz[ibj]=bjets_pz[ibj];
      			je[ibj]=bjets_e[ibj];
      			

      			pjpx[ibj]=p_bjets_px[ibj];
      			pjpy[ibj]=p_bjets_py[ibj];
      			pjpz[ibj]=p_bjets_pz[ibj];
      			pje[ibj]=p_bjets_e[ibj];
      			double pull=fabs(pull_thetas[ibj]/acos(-1.0)),pull2=fabs(p_pull_thetas[ibj]/acos(-1.0));
				 //double pulldiff;
				 pulldiff = pull2-pull;
				 if(pulldiff>1.0){
				 	pulldiff-=2.0;
				 }
				 if(pulldiff<-1.0)
				 {
				 	pulldiff+=2.0;
				 }
				//h_etapull.Fill(bjets_eta[ibj],pull);
				//if(bjets_eta[ibj]>2.5 && bjets_eta[ibj]<4){h_pullr2d.Fill(bjets_eta[ibj],pull,xsecweight);}
         switch(ibj){
            case 0: h_pull_j1.Fill(pull,weight); //h_pull2_j1.Fill(fabs(pull));
            		//h_etapull1.Fill(jdiff1.Eta(),pull);
            		//h_petapull1.Fill(jdiffp1.Eta(),pull2);
            		h_pulld1.Fill(pull,pulldiff,weight);
            		h_pullp_j1.Fill(pull2,weight);
            		h_eta1.Fill(jdiff1.Eta(),weight);
            		h_etap1.Fill(jdiffp1.Eta(),weight);
            		jpull[ibj]=pull;
            		jpullp[ibj]=pull2;
					if(bjets_eta[ibj]>2.8 && bjets_eta[ibj]<4){
						h_pullr_j1.Fill(pull,weight);
						h_pullpr_j1.Fill(pull2,weight);
					}
					if(fabs(pulldiff)>0.1){
						h_pulld1g.Fill(pull,pulldiff);
						h_etadiff1.Fill(jdiff1.Eta(),pulldiff,weight);
					}
					if(fabs(pulldiff)>0.1){
						h_pullg_j1.Fill(pull2,weight);
					}
					else if(fabs(pulldiff)<=0.1){
						h_pulls_j1.Fill(pull2,weight);
					}
					break;
            case 1: h_pull_j2.Fill(pull,weight); //h_pull2_j1.Fill(fabs(pull));
            		//h_etapull2.Fill(jdiff2.Eta(),pull);
            		//h_petapull2.Fill(jdiffp2.Eta(),pull2);
            		h_pulld2.Fill(pull,pulldiff,weight);
            		h_pullp_j2.Fill(pull2,weight);
            		h_eta2.Fill(jdiff2.Eta(),weight);
            		h_etap2.Fill(jdiffp2.Eta(),weight);
            		jpull[ibj]=pull;
            		jpullp[ibj]=pull2;
					if(bjets_eta[ibj]>2.8 && bjets_eta[ibj]<4){
						h_pullr_j2.Fill(pull,weight);
						h_pullpr_j2.Fill(pull2,weight);
					}
					if(fabs(pulldiff)>0.1){
						h_pulld2g.Fill(pull,pulldiff,weight);
						h_etadiff2.Fill(jdiff2.Eta(),pulldiff,weight);
					}
					if(fabs(pulldiff)>0.1){
						h_pullg_j2.Fill(pull2,weight);
					}
					else if(fabs(pulldiff)<=0.05){
						h_pulls_j2.Fill(pull2,weight);
					}
					break;
         }
         gfraction_pt[ibj]=good_fraction_pt[ibj];
      }

      mynewtree->Fill();
   }
   //myfile->Close();
  myfile->Close();



   nentries = mynewtree->GetEntries();
   mynewtree->SetBranchAddress("eta",jeta);
   mynewtree->SetBranchAddress("p_eta",pjeta);
   mynewtree->SetBranchAddress("pull",jpull);
   mynewtree->SetBranchAddress("pullp",jpullp);
   //mynewtree->SetBranchAddress("")


   for (Long64_t i = 0; i < nentries; ++i)
   {
   		mynewtree->GetEntry(i);
   		
      for(int ibj=0;ibj<nbjets;++ibj){
				
		double etaweight,etapweight;
         switch(ibj){
            case 0: //h_pull_j1.Fill(pull); //h_pull2_j1.Fill(fabs(pull));
            		etaweight = h_eta1.GetBinContent(h_eta1.FindBin(jeta[ibj]));
            		etapweight = h_etap1.GetBinContent(h_etap1.FindBin(pjeta[ibj]));
            		h_etapull1.Fill(jeta[ibj],jpull[ibj],1/etaweight);
            		h_petapull1.Fill(pjeta[ibj],jpullp[ibj],1/etapweight);
            		
					break;
            case 1: etaweight = h_eta2.GetBinContent(h_eta2.FindBin(jeta[ibj]));
            		etapweight = h_etap2.GetBinContent(h_etap2.FindBin(jeta[ibj]));
            		h_etapull2.Fill(jeta[ibj],jpull[ibj],1/etaweight);
            		h_petapull2.Fill(pjeta[ibj],jpullp[ibj],1/etapweight);
					break;
         }
      }
   }
  
   newfile->cd();
   mynewtree->Write();
   h_pull_j1.Write(); //h_pull2_j1.Fill(fabs(pull));
   h_pull_j2.Write();

   h_pullr_j1.Write();
   h_pullr_j2.Write();

   h_etapull1.Write();
   h_etapull2.Write();


   h_pullp_j1.Write();
   h_pullp_j2.Write();

   h_pullpr_j1.Write();
   h_pullpr_j2.Write();

   h_pullg_j1.Write();
   h_pullg_j2.Write();
   
   h_pulls_j1.Write();
   h_pulls_j2.Write();

  	h_petapull1.Write();
   h_petapull2.Write();

   h_pulld1.Write();
   h_pulld2.Write();

   h_pulld1g.Write();
   h_pulld2g.Write();

   h_etadiff1.Write();
   h_etadiff2.Write();
		
 //end of mc analysis
 }





	
 
	
   //TTree *newtree = new TTree("T","T");
	
   double_t norm;

	/*h_pullm2.Sumw2();
   norm = h_pullm2.Integral();
   h_pullm2.Scale(1/norm);

	 h_pull2_j1.Sumw2();
   norm = h_pull2_j1.Integral();
   h_pull2_j1.Scale(1/norm);*/

   //h_pull_j1.Sumw2();
   //norm = h_pull_j1.Integral();
   //h_pull_j1.Scale(1/norm);

	 /*h_pull2_j2.Sumw2();
   norm = h_pull2_j2.Integral();
   h_pull2_j2.Scale(1/norm);*/   

   //h_pull_j2.Sumw2();
   //norm = h_pull_j2.Integral();
   //h_pull_j2.Scale(1/norm);   

   //h_pullr_j1.Sumw2();
   //norm = h_pullr_j1.Integral();
   //h_pullr_j1.Scale(1/norm);

   //h_pullr_j2.Sumw2();
   //norm = h_pullr_j2.Integral();
   //h_pullr_j2.Scale(1/norm);
//h_etapull.Sumw2();


   

   


   //h_pull2_j1.Fill(fabs(pull));
   
  newfile->Close();
   //delete oldfile;
}
