#define Plotter_cxx
#include "Plotter.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TColor.h>

void Plotter::Loop()
{
//   In a ROOT session, you can do:
//      root> .L Plotter.C
//      root> Plotter t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();
   
   int numbins=80;
   TH1D h1("j1pull_angle","j1pull_angle",numbins,-acos(-1),acos(-1));
   TH1D h2("j2pull_angle","j2pull_angle",numbins,-acos(-1),acos(-1));
   TH2D h1v("j1pullrap","j1pullphi",numbins,-0.06,0.06,numbins,-0.06,0.06);
   //TH1D h1p("j1pullphi","j1pullphi",numbins,-0.15,0.15);
   TH2D h2v("j2pullrap","j2pullphi",numbins,-0.06,0.06,numbins,-0.06,0.06);
   //TH1D h2p("j2pullphi","j2pullphi",numbins,-0.15,0.15);
   TH2D h21("pull_angle","pull_angle",numbins,-acos(-1),acos(-1),numbins,3.2e4,8.0e4);
   //TH2D h22("pull_angle","pull_angle",numbins,-acos(-1),acos(-1),
      //numbins,0,1e4);
   Long64_t nbytes = 0, nb = 0;
   double norm,scale;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // no. of tracks condition?
      if((j1eta- 2*j1deta)<3.0 && (j1eta+ 2*j1deta)>4.0)
         continue;
      if((j2eta- 2*j2deta)<3.0 && (j2eta+ 2*j2deta)>4.0)
         continue;

      // if (Cut(ientry) < 0) continue;
      h1.Fill(j1theta);
      h2.Fill(j2theta);
      h21.Fill(j1theta,j1j2M);
      //h22.Fill(j2theta,j1j2M);
      h1v.Fill(j1pull_rap,j1pull_phi);
      //h1r.Fill(j1pull_rap);
      h2v.Fill(j2pull_rap,j2pull_phi);
      //h2r.Fill(j2pull_rap);
   }
   const Int_t NRGBs = 5;
    const Int_t NCont = 500;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);

   TCanvas c,c1,c2;
   c.Divide(1,1);
   c.cd(1);
   //(c.GetPad(1))->SetLogy();
   h1.GetYaxis()->SetMoreLogLabels(kTRUE);
   h1.SetLineColor(kRed);
   h1.SetLabelSize(22,"x");
   h1.SetLabelSize(22,"y");
   //h1.SetAxisRange(0., 1.,"Y");
   h1.GetYaxis()->SetTitleOffset(0.2);
   h1.GetXaxis()->SetTitle("Pull angle");
   //h1.GetYaxis()->SetTitle("Events");
   norm = h1.Integral();
   scale =1/norm;
   h1.Scale(scale,"width");
   h1.GetYaxis()->SetRangeUser(0,0.25);
   
   h1.Draw();
   h2.SetLineColor(kBlue);
   norm = h2.Integral();
   scale = 1/norm;
   h2.Scale(scale,"width");
   h2.Draw("same");
   c.SaveAs("pull100.png");

   
    

   c1.Divide(2,1);
   c1.cd(1);
   //(c1.GetPad(1))->SetLogy();
   //h1r.GetYaxis()->SetMoreLogLabels(kTRUE);
   //h1r.SetLineColor(kRed);
   h1v.SetLabelSize(15,"x");
   h1v.SetLabelSize(15,"y");
   h1v.GetYaxis()->SetTitle("Azimuthal Angle");
   h1v.GetXaxis()->SetTitle("Rapidity");
   //gStyle->SetPalette(1);

   h1v.Draw("COLZ");

   c1.Update();

   c1.cd(2);
   //(c1.GetPad(2))->SetLogy();
   //h1p.GetYaxis()->SetMoreLogLabels(kTRUE);
   h2v.SetLabelSize(15,"x");
   h2v.SetLabelSize(15,"y");
   //h1.SetAxisRange(0., 1.,"Y");
   //h1.SetLabelOffset(0.008,"y");
   h2v.GetYaxis()->SetTitle("Azimuthal Angle");
   h2v.GetXaxis()->SetTitle("Rapidity");
   //gStyle->SetPalette(1);
   h2v.Draw("COLZ");
   c1.SaveAs("pull_vec100.png");

   Int_t  test;
   c2.Divide(1,1);
   c2.cd(1);
   /*(c2.GetPad(1))->SetLogy();
   h21.GetYaxis()->SetMoreLogLabels(kTRUE);*/
   h21.SetLabelSize(22,"y");
   h21.SetLabelSize(22,"x");
   //h21.SetLabelOffset(0.004,"y");
   
   h21.GetXaxis()->SetTitle("Pull angle");
   h21.GetYaxis()->SetTitle("Invariant Mass [eV]");

   /*Int_t MyPalette[100];
   Double_t Red[]    = {0., 0.0, 1.0, 1.0, 1.0};
   Double_t Green[]  = {0., 0.0, 0.0, 1.0, 1.0};
   Double_t Blue[]   = {0., 1.0, 0.0, 0.0, 1.0};
   Double_t Length[] = {0., .1, .25, .75, 1.0};
   Int_t FI = TColor::CreateGradientColorTable(5, Length, Red, Green, Blue, 100);
   test=FI;
   for (int i=0;i<100;i++) MyPalette[i] = FI+i;
   
   //test=TColor::CreateGradientColorTable(Number,Length,Red,Green,Blue,nob);
   gStyle->SetPalette(100,MyPalette);*/
   
   /*const Int_t NRGBs = 5;
    const Int_t NCont = 255;

    Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    test = TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);*/
    //double norm;
   //gStyle->SetPalette(1);
   h21.Draw("COLZ");
   c2.SaveAs("INVMASS100.png");
   cout<<"boobo\n"<<test<<endl;
}