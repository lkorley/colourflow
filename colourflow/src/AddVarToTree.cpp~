#define AddVarToTree_cxx
#include "AddVarToTree.hpp"

using namespace std;

AddVarToTree::AddVarToTree(vector<const char*>& fileNames, const char* path, 
			   const char* treeName):
  path_(path),treeName_(treeName){
  for(size_t i=0;i<fileNames.size();i++)
    {
      cout<<"Constructing: AddVarToTree..."<<" Opening file: "
	  <<fileNames[i]<<" and dir: "<<path<<endl;
      // push back the file name
      fileNames_.push_back(fileNames[i]);
    }
  
  // get the root files
  getFiles();
}
//---------------------------------
//
//---------------------------------
void AddVarToTree::addInterestingVars()
{
  // names of interesting vars to store to tree
  vector<string> vec_var;
  vec_var.push_back("j1et");
  vec_var.push_back("j2et");
  vec_var.push_back("j1eta");
  vec_var.push_back("j2eta");
  vec_var.push_back("j1px");
  vec_var.push_back("j1py");
  vec_var.push_back("j1pz");
  vec_var.push_back("j1e");
  vec_var.push_back("j2px");
  vec_var.push_back("j2py");
  vec_var.push_back("j2pz");
  vec_var.push_back("j2e");
  for(size_t i=0;i<files_.size();i++)
  {
      // initilise vector of var names and values
      vector<pair<string,double> > vec_var_val;
      double* dummyValue=0;
      for(size_t v=0;v<vec_var.size();v++)
	     {
	       dummyValue=new double(0.);
	       vec_var_val.push_back(make_pair(vec_var[v],0.));
	     }
      
      // get the original tree from the file
      TTree* theTree=0;
      if(files_[i]&&files_[i]->IsOpen())
        files_[i]->GetObject((treeName_).c_str(),theTree);
        //files_[i]->GetObject((path_+"/"+treeName_).c_str(),theTree);
      else {
        cout<<"Error opening file: "<<fileNames_[i]<<endl;
	      continue;
      }
      bool isData        = (TString(files_[i]->GetName()).Contains("data")==true);
      Long64_t nEntries  = theTree->GetEntries();
      
      // initialise relevant branches to calculate things...
      double px_jet[100],py_jet[100],pz_jet[100],e_jet[100],vz_jet[100];
      double px_mcjet[100],py_mcjet[100],pz_mcjet[100],e_mcjet[100];
      double hasB_mcjet[100],vx_jet[100],vy_jet[100];
      double fracIDs_topo_jet[100],cpf_jet[100],mtf_jet[100];
      double width_jet[100],nUpstr_jet[100],nTrks_jet[100];
      double n90_jet[100],matched_topo_ind_jet[100];
      
      UInt_t n_jet,n_mcjet,n_trk_jet_daught,n_ntr_jet_daught,n_bQuarks;
      double px_b[100],py_b[100],pz_b[100],e_b[100],pdgId_b[100];
      double px_trk_jet_daught[200],py_trk_jet_daught[200],pz_trk_jet_daught[200];
      double pid_trk_jet_daught[200],ipchi2_pv_trk_jet_daught[200];
      double jet_index_trk_jet_daught[200],chi2_trk_jet_daught[200];
      double ghostProb_trk_jet_daught[200],ndof_trk_jet_daught[200];
      double jet_index_ntr_jet_daught[200],e_ntr_jet_daught[200];
      double topo_doca0_trk_jet_daught[200],topo_doca1_trk_jet_daught[200];
      double topo_doca2_trk_jet_daught[200],topo_doca3_trk_jet_daught[200];
      double muonInAcc_trk_jet_daught[200],inTopo_trk_jet_daught[200],topo_index_trk_jet_daught[200];
      double history_trk_jet_daught[200],kaon_DLL_trk_jet_daught[200];
      double Btopo_px[2],Btopo_py[2],Btopo_pz[2],Btopo_e[2];
      double Btopo_vx[2],Btopo_vy[2],Btopo_vz[2];
      double Bc_pvx[1],Bc_pvy[1],Bc_pvz[1];

      theTree->SetBranchAddress("n_jet",&n_jet);
      theTree->SetBranchAddress("n_mcjet",&n_mcjet);
      theTree->SetBranchAddress("n_trk_jet_daught",&n_trk_jet_daught);
      theTree->SetBranchAddress("n_ntr_jet_daught",&n_ntr_jet_daught);
      theTree->SetBranchAddress("n_bQuarks",&n_bQuarks);
      theTree->SetBranchAddress("Btopo_px",Btopo_px);
      theTree->SetBranchAddress("Btopo_py",Btopo_py);
      theTree->SetBranchAddress("Btopo_pz",Btopo_pz);
      theTree->SetBranchAddress("Btopo_vx",Btopo_vx);
      theTree->SetBranchAddress("Btopo_vy",Btopo_vy);
      theTree->SetBranchAddress("Btopo_vz",Btopo_vz);
      theTree->SetBranchAddress("Bc_pvx",Bc_pvx);
      theTree->SetBranchAddress("Bc_pvy",Bc_pvy);
      theTree->SetBranchAddress("Bc_pvz",Bc_pvz);
      theTree->SetBranchAddress("Btopo_e",Btopo_e);
      theTree->SetBranchAddress("px_jet",px_jet);
      theTree->SetBranchAddress("py_jet",py_jet);
      theTree->SetBranchAddress("pz_jet",pz_jet);
      theTree->SetBranchAddress("vx_jet",vx_jet);
      theTree->SetBranchAddress("vy_jet",vy_jet);
      theTree->SetBranchAddress("vz_jet",vz_jet);
      theTree->SetBranchAddress("e_jet",e_jet);
      theTree->SetBranchAddress("matched_topo_ind_jet",matched_topo_ind_jet);
      theTree->SetBranchAddress("hasB_mcjet",hasB_mcjet);
      theTree->SetBranchAddress("px_mcjet",px_mcjet);
      theTree->SetBranchAddress("py_mcjet",py_mcjet);
      theTree->SetBranchAddress("pz_mcjet",pz_mcjet);
      theTree->SetBranchAddress("e_mcjet",e_mcjet);
      theTree->SetBranchAddress("fracIDs_topo_jet",fracIDs_topo_jet);
      theTree->SetBranchAddress("cpf_jet",cpf_jet);
      theTree->SetBranchAddress("mtf_jet",mtf_jet);
      theTree->SetBranchAddress("nTrks_jet",nTrks_jet);
      theTree->SetBranchAddress("n90_jet",n90_jet);
      theTree->SetBranchAddress("nUpstr_jet",nUpstr_jet);
      theTree->SetBranchAddress("width_jet",width_jet);
      theTree->SetBranchAddress("px_b",px_b);
      theTree->SetBranchAddress("py_b",py_b);
      theTree->SetBranchAddress("pz_b",pz_b);
      theTree->SetBranchAddress("e_b",e_b);
      theTree->SetBranchAddress("pdgId_b",pdgId_b);
      theTree->SetBranchAddress("n_trk_jet_daught", &n_trk_jet_daught);
      theTree->SetBranchAddress("px_trk_jet_daught", px_trk_jet_daught);
      theTree->SetBranchAddress("py_trk_jet_daught", py_trk_jet_daught);
      theTree->SetBranchAddress("pz_trk_jet_daught", pz_trk_jet_daught);
      theTree->SetBranchAddress("doca_topo_trk_0_trk_jet_daught",topo_doca0_trk_jet_daught);
      theTree->SetBranchAddress("doca_topo_trk_1_trk_jet_daught",topo_doca1_trk_jet_daught);
      theTree->SetBranchAddress("doca_topo_trk_2_trk_jet_daught",topo_doca2_trk_jet_daught);
      theTree->SetBranchAddress("doca_topo_trk_3_trk_jet_daught",topo_doca3_trk_jet_daught);
      theTree->SetBranchAddress("topo_index_trk_jet_daught",topo_index_trk_jet_daught);
      theTree->SetBranchAddress("ipchi2_pv_trk_jet_daught", ipchi2_pv_trk_jet_daught);
      theTree->SetBranchAddress("pid_trk_jet_daught", pid_trk_jet_daught);
      theTree->SetBranchAddress("jet_index_trk_jet_daught", jet_index_trk_jet_daught);
      theTree->SetBranchAddress("ghostProb_trk_jet_daught", ghostProb_trk_jet_daught);
      theTree->SetBranchAddress("chi2_trk_jet_daught", chi2_trk_jet_daught);
      theTree->SetBranchAddress("ndof_trk_jet_daught", ndof_trk_jet_daught);
      theTree->SetBranchAddress("e_ntr_jet_daught",e_ntr_jet_daught);
      theTree->SetBranchAddress("jet_index_ntr_jet_daught",jet_index_ntr_jet_daught);
      theTree->SetBranchAddress("muonInAcc_trk_jet_daught",muonInAcc_trk_jet_daught);
      theTree->SetBranchAddress("inTopo_trk_jet_daught",inTopo_trk_jet_daught);
      theTree->SetBranchAddress("history_trk_jet_daught",history_trk_jet_daught);
      theTree->SetBranchAddress("kaonDLL_trk_jet_daught",kaon_DLL_trk_jet_daught);
      for(size_t v=0;v<vec_var_val.size();v++)
	   {
	       if(theTree->FindBranch(vec_var_val[v].first.c_str())){
	         cout<<"AddVarToTree:addInterestingVars() : WARNING "
		        <<"Branch with base name '"<<vec_var_val[v].first<<"' already exists! "
		        <<"Will overwrite...!"<<endl;
	           cout<<endl;
	           theTree->SetBranchStatus(vec_var_val[v].first.c_str(),0);
	       }
	   }
      // done initialising original tree

      // now initialise the new tree that contains only the useful information
      TString newFileName=fileNames_[i];
      newFileName.ReplaceAll(".root","newVars.root");
      TFile* newFile=new TFile(newFileName,"RECREATE");
      newFile->mkdir(path_.c_str());
      newFile->cd(path_.c_str());
      // turn all branches from original tree off before cloning
      theTree->SetBranchStatus("*",0);
      theTree->SetBranchStatus("runNumber",1);
      theTree->SetBranchStatus("evtNumber",1);
      theTree->SetBranchStatus("nPVs",1);
      if(theTree->FindBranch("xsec_weight"))
        theTree->SetBranchStatus("xsec_weight",1);
      TTree* newTree=theTree->CloneTree();
      // now turn them all back on again
      theTree->SetBranchStatus("*",1);
  
      for(size_t v=0;v<vec_var_val.size();v++)
	   {
	     newTree->Branch(vec_var_val[v].first.c_str(),
			  &vec_var_val[v].second,
			  TString(vec_var_val[v].first+"/D").Data());
	     cout<<"AddVarToTree::addInterestingVars() : Will add variable: '"
	      <<vec_var_val[v].first<<"'"<<endl;
	     cout<<endl;
	   }
      
      // now loop over theTree entries
      vector<double> vec_val;
      newFile->cd();
    for(Long64_t iEnt=0; iEnt<50; iEnt++) // changed nEntries to 50
	 {
	     theTree->GetEntry(iEnt);
	      double j1et=-100.,j2et=-100.,j1eta=-100.,j2eta=-100.;
	     double j1px=-100.,j1py=-100.,j1pz=-100.,j1e=-100.;
	      double j2px=-100.,j2py=-100.,j2pz=-100.,j2e=-100.;
	      double j1j2dphi=-100.;
	

	      vector<TLorentzVector> good_jets;
	      vector<int> good_jets_indx;
	  
	      // pick the leading b jets
	     for(size_t ijet=0;ijet<2;ijet++)
       {
	 	      if(fracIDs_topo_jet[ijet]>=0.6){
		        good_jets.push_back(TLorentzVector(px_jet[ijet],py_jet[ijet],
						   pz_jet[ijet],e_jet[ijet]));
		        good_jets_indx.push_back(ijet);
	        }
	      }

	      // only store events where we have two good
	     // jets and two good mc jets
	     if(isData==true&&good_jets.size()==2){
	       TLorentzVector jet1=good_jets[0];
	       TLorentzVector jet2=good_jets[1];
	       j1j2dphi    = fabs(jet1.Phi()-jet2.Phi());
	       if(j1j2dphi>acos(-1.))
	           j1j2dphi  = 2*acos(-1.)-j1j2dphi;
	    
	    // additional track based info for jets
	    // LUKE ADDS STUFF IN THIS LOOP
	       for(size_t t=0;t<n_trk_jet_daught;t++)
         {
            TVector3 trk_v(px_trk_jet_daught[t],py_trk_jet_daught[t],pz_trk_jet_daught[t]);
            double mass=139.6;
            if(fabs(pid_trk_jet_daught[t])==11)
               mass=0.510;
            else if(fabs(pid_trk_jet_daught[t])==13)
               mass=105.7;
            else if(fabs(pid_trk_jet_daught[t])==321)
		  	       mass=493.;
           else if(fabs(pid_trk_jet_daught[t])==2212)
             mass=493.;
			     if(kaon_DLL_trk_jet_daught[t]>-5)
		  	      mass=493.;
			     TLorentzVector trk_lv(trk_v,sqrt(trk_v.Mag2()+mass*mass));
			     TLorentzVector trk_lv_topoLike(trk_v,sqrt(trk_v.Mag2()+493.*493.));
			     if(trk_lv.Pt()>100.&&trk_lv.P()>1000.&&
            ghostProb_trk_jet_daught[t]<0.4&&
            (chi2_trk_jet_daught[t]/ndof_trk_jet_daught[t])<3.&&
            history_trk_jet_daught[t]==3)
            {
		  		// these are the tracks you want to consider for colour pull
		  		// need to keep track of which jet each track is coming from
		  		// and calculate the colour pull variable 
		  		// create struct to hold tracks in a jet and jet vector !DONE!
		  		// need function or operations to assign tracks to jet struct
				// if jet_index_daught_trk[t] == either of good jets
				// add trk t to jet_trks variable
			       }	
           }
	       j1px        = jet1.Px();
	       j1py        = jet1.Py();
	       j1pz        = jet1.Pz();
	       j1e         = jet1.E();
         j1et        = jet1.Et();
         j1eta       = jet1.Eta();
         j2px        = jet2.Px();
	       j2py        = jet2.Py();
	       j2pz        = jet2.Pz();
	       j2e         = jet2.E();
	       j2et        = jet2.Et();
         j2eta       = jet2.Eta();
	     }
	  // correctly fill the vector of variable/values
	  for(size_t v=0;v<vec_var.size();v++)
	   	{
	      if(vec_var[v]=="j1px")
		vec_var_val[v].second=j1px;
	      if(vec_var[v]=="j1py")
		vec_var_val[v].second=j1py;
	      if(vec_var[v]=="j1pz")
		vec_var_val[v].second=j1pz;
	      if(vec_var[v]=="j1e")
		vec_var_val[v].second=j1e;
	      if(vec_var[v]=="j1et")
	        vec_var_val[v].second=j1et;
	      if(vec_var[v]=="j1eta")
	        vec_var_val[v].second=j1eta;
	  	  
	      if(vec_var[v]=="j2px")
	        vec_var_val[v].second=j2px;
	      if(vec_var[v]=="j2py")
	        vec_var_val[v].second=j2py;
	      if(vec_var[v]=="j2pz")
	        vec_var_val[v].second=j2pz;
	      if(vec_var[v]=="j2e")
		  vec_var_val[v].second=j2e;
	      if(vec_var[v]=="j2et")
		vec_var_val[v].second=j2et;
	      if(vec_var[v]=="j1eta")
		vec_var_val[v].second=j1eta;
	      if(vec_var[v]=="j2eta")
		vec_var_val[v].second=j2eta;

	   	}
	  
	    for(size_t v=0;v<vec_var_val.size();v++)
	    {
	      TBranch* br_var=newTree->GetBranch(vec_var_val[v].first.c_str());
	      br_var->Fill();
	    }
    }
    newTree->Write();
    newFile->Close();
    delete dummyValue;
    delete newFile;
  }
}
//---------------------------------
// 
//---------------------------------
void AddVarToTree::addJetEtSystWeight()
{
  for(size_t i=0;i<files_.size();i++)
    {
      // get the original tree from the file
      TTree* theTree=0;
      if(files_[i]&&files_[i]->IsOpen())
	files_[i]->GetObject((treeName_).c_str(),theTree);
      else {
	cout<<"AddVarToTree::addJetEtSystWeight() Error opening file: "<<fileNames_[i]<<endl;
	continue;
      }
      TString fileNameStr(fileNames_[i]);
      if(!fileNameStr.Contains("qcd")&&!fileNameStr.Contains("mc")){
	cout<<"AddVarToTree::addJetEtSystWeight() WARNING: This is not "
	    <<"a qcd mc file. Will Skip..."<<endl;
	continue;
      }
      Long64_t nEntries  = theTree->GetEntries();
      double b1et=-100.,b2et=-100.;
      if(!theTree->FindBranch("j1et")||
	 !theTree->FindBranch("j2et")){
	theTree->SetBranchAddress("b1et",&b1et);
	theTree->SetBranchAddress("b2et",&b2et);
      }
      else{
	theTree->SetBranchAddress("j1et",&b1et);
	theTree->SetBranchAddress("j2et",&b2et);
      }
     
      // initialise relevant branches to calculate helicity
      if(theTree->FindBranch("j1etj2et_weight")){
	cout<<"AddVarToTree:addJetEtSystWeight() : WARNING "
	    <<"Branch with base name 'mbb_weight' already exists! "
	    <<"Will overwrite...!"<<endl;
	theTree->SetBranchStatus("j1etj2et_syst_weight",0);
      }
      
      TFile* frew=TFile::Open("corr_j1etj2et.root");
      if(!frew){
	cout<<"AddVarToTree:addJetEtSystWeight() : ERROR "
	    <<"Reweighting file corr_j1etj2et.root does not exist "
	    <<"Exiting...!"<<endl;
	return;
      }
      TH2D* h_corr_j1etj2et=(TH2D*)frew->Get("hr_j1etj2et");
      if(!h_corr_j1etj2et){
	cout<<"AddVarToTree:addJetEtSystWeight() : ERROR "
	    <<"Reweighting histo h_corr_j1j2et does not exist "
	    <<"Exiting...!"<<endl;
	return;
      }

      TFile* newFile=new TFile(fileNames_[i].c_str(),"RECREATE");
      newFile->mkdir(path_.c_str());
      newFile->cd(path_.c_str());
      TTree* newTree=theTree->CloneTree();
      double j1etj2et_syst_weight=1;

      newTree->Branch("j1etj2et_syst_weight",&j1etj2et_syst_weight,
		      "j1etj2et_syst_weight/D");
      cout<<"AddVarToTree::addJetEtSystWeight() : Will add variable: 'j1etj2et_syst_weight'"
	  <<endl;
     
     
      // loop over theTree entries
      vector<double> vec_val;
      for(Long64_t iEnt=0; iEnt<nEntries; iEnt++)
	{
	  theTree->GetEntry(iEnt); 
	  int ibin=h_corr_j1etj2et->FindBin(b1et/1000.,b2et/1000.);
	  j1etj2et_syst_weight=h_corr_j1etj2et->GetBinContent(ibin);
	  // cap the correction to 9
	  if(j1etj2et_syst_weight>9)
	    j1etj2et_syst_weight=9;
	  TBranch* br_var=newTree->GetBranch("j1etj2et_syst_weight");
	  br_var->Fill();
	}
      newTree->Write();
      newFile->Close();
      delete newFile;
    }
}
//---------------------------------
// 
//---------------------------------
void AddVarToTree::addMbbSystWeight()
{
  for(size_t i=0;i<files_.size();i++)
    {
      // get the original tree from the file
      TTree* theTree=0;
      if(files_[i]&&files_[i]->IsOpen())
	files_[i]->GetObject((treeName_).c_str(),theTree);
      else {
	cout<<"AddVarToTree::addQcdWeight() Error opening file: "<<fileNames_[i]<<endl;
	continue;
      }
      TString fileNameStr(fileNames_[i]);
      if(!fileNameStr.Contains("qcd")&&!fileNameStr.Contains("mc")){
	cout<<"AddVarToTree::addMbbSystWeight() WARNING: This is not "
	    <<"a qcd mc file. Will Skip..."<<endl;
	continue;
      }
      Long64_t nEntries  = theTree->GetEntries();
       double b1b2mass=-100.;
      theTree->SetBranchAddress("b1b2mass",&b1b2mass);
     
      // initialise relevant branches to calculate helicity
      if(theTree->FindBranch("mbb_weight")){
	cout<<"AddVarToTree:addMbbSystWeight() : WARNING "
	    <<"Branch with base name 'mbb_weight' already exists! "
	    <<"Will overwrite...!"<<endl;
	theTree->SetBranchStatus("mbb_syst_weight",0);
      }
      
      TFile* frew=TFile::Open("corr_mass.root");
      if(!frew){
	cout<<"AddVarToTree:addMbbSystWeight() : ERROR "
	    <<"Reweighting file corr_mass.root does not exist "
	    <<"Exiting...!"<<endl;
	return;
      }
      TH2F* h_corr_mass=(TH2F*)frew->Get("h_corr_mass");
      if(!h_corr_mass){
	cout<<"AddVarToTree:addMbbSystWeight() : ERROR "
	    <<"Reweighting histo h_corr_mass does not exist "
	    <<"Exiting...!"<<endl;
	return;
      }

      TFile* newFile=new TFile(fileNames_[i].c_str(),"RECREATE");
      newFile->mkdir(path_.c_str());
      newFile->cd(path_.c_str());
      TTree* newTree=theTree->CloneTree();
      double mbb_syst_weight=1;

      newTree->Branch("mbb_syst_weight",&mbb_syst_weight,"mbb_syst_weight/D");
      cout<<"AddVarToTree::addXsecWeight() : Will add variable: 'mbb_syst_weight'"
	  <<endl;
     
     
      // loop over theTree entries
      vector<double> vec_val;
      for(Long64_t iEnt=0; iEnt<nEntries; iEnt++)
	{
	  theTree->GetEntry(iEnt); 
	  int ibin=h_corr_mass->FindBin(b1b2mass/1000.);
	  mbb_syst_weight=h_corr_mass->GetBinContent(ibin);
	  TBranch* br_var=newTree->GetBranch("mbb_syst_weight");
	  br_var->Fill();
	}
      newTree->Write();
      newFile->Close();
      delete newFile;
    }
}
//---------------------------------
// 
//---------------------------------
void AddVarToTree::addXsecWeight()
{
  for(size_t i=0;i<files_.size();i++)
    {
      // get the original tree from the file
      TTree* theTree=0;
      if(files_[i]&&files_[i]->IsOpen())
	files_[i]->GetObject((treeName_).c_str(),theTree);
      else {
	cout<<"AddVarToTree::addQcdWeight() Error opening file: "<<fileNames_[i]<<endl;
	continue;
      }
      TString fileNameStr(fileNames_[i]);
      if(!fileNameStr.Contains("qcd")&&!fileNameStr.Contains("mc")){
	cout<<"AddVarToTree::addQcdWeight() WARNING: This is not "
	    <<"a qcd mc file. Will Skip..."<<endl;
	continue;
      }
      Long64_t nEntries  = theTree->GetEntries();
      
      // initialise relevant branches to calculate helicity
      if(theTree->FindBranch("xsec_weight")){
	cout<<"AddVarToTree:addXsecWeight() : WARNING "
	    <<"Branch with base name 'xsec_weight' already exists! "
	    <<"Will overwrite...!"<<endl;
	theTree->SetBranchStatus("xsec_weight",0);
      }
      
      TFile* newFile=new TFile(fileNames_[i].c_str(),"RECREATE");
      newFile->mkdir(path_.c_str());
      newFile->cd(path_.c_str());
      TTree* newTree=theTree->CloneTree();
      double xsec_weight=1;
      // units are ub!
      newTree->Branch("xsec_weight",&xsec_weight,"xsec_weight/D");
      cout<<"AddVarToTree::addXsecWeight() : Will add variable: 'xsec_weight'"
	  <<endl;
      if(fileNameStr.Contains("minbias")){
	if(fileNameStr.Contains("2011")){
	  if(fileNameStr.Contains("0_20")||fileNameStr.Contains("30000041"))
	    xsec_weight=7.8e-1*0.00192/1034996.;           
	  else if(fileNameStr.Contains("20_40")||fileNameStr.Contains("30000042"))
	    xsec_weight=2.9e-4*0.0051/2055993.;   //4.5662e-3/1035495.      
	  else if(fileNameStr.Contains("40_60")||fileNameStr.Contains("30000043"))
	    xsec_weight=1.7e-5*0.0065/1964995.;   //0.393391e-3/1035999.    
	  else if(fileNameStr.Contains("60_120")||fileNameStr.Contains("30000044"))
	    xsec_weight=3.8e-6*0.0058/2022243.;   //0.082786e-3/1033996.     
	  else if(fileNameStr.Contains("120")||fileNameStr.Contains("30000045"))
	    xsec_weight=1.8e-7*0.0047/2010742.;   //0.00446798e-3/1028747.   
	}
	else if(fileNameStr.Contains("2012")){
	  if(fileNameStr.Contains("0_20")||fileNameStr.Contains("30000041"))
	    xsec_weight=7.8e-1;           
	  else if(fileNameStr.Contains("20_40")||fileNameStr.Contains("30000042"))
	    xsec_weight=2.9e-4*0.0051/2055993.;    //4.5662e-3/1035495.      
	  else if(fileNameStr.Contains("40_60")||fileNameStr.Contains("30000043"))
	    xsec_weight=1.7e-5*0.0065/1964995.;   //0.393391e-3/1035999.     
	  else if(fileNameStr.Contains("60_120")||fileNameStr.Contains("30000044"))
	    xsec_weight=3.8e-6*0.0058/2022243.;    //0.082786e-3/1033996.     
	  else if(fileNameStr.Contains("120")||fileNameStr.Contains("30000045"))
	    xsec_weight=1.8e-7*0.0047/2010742.;   //0.00446798e-3/1028747.   
	}
      }
      else if(fileNameStr.Contains("ggh")){
	if(fileNameStr.Contains("2011")){
	  //x-sec x br(bb) x dec prod
	  xsec_weight=15.13e-12 * 0.58 * 0.051/1014496.; 
	}
	if(fileNameStr.Contains("2012")){
	  xsec_weight=19.27e-12 * 0.58 * 0.060/979997.;
	}
      }
      else if(fileNameStr.Contains("ttbar_gg_ljets")){
	if(fileNameStr.Contains("2011")){
	  //x-sec x br(bb) x dec prod
	  xsec_weight=144e-12 * 0.05 / 2031996.;
	}
	else if(fileNameStr.Contains("2011")){
	  //x-sec x br(bb) x dec prod
	  xsec_weight=144e-12 * 0.05 / 2031996.;
	}
      }
      else if(fileNameStr.Contains("ttbar_qq_ljets")){
	if(fileNameStr.Contains("2011")){
	  //x-sec x br(bb) x dec prod
	  xsec_weight=16e-12 * 0.07 / 2031996.;
	}
	else if(fileNameStr.Contains("2011")){
	  //x-sec x br(bb) x dec prod
	  xsec_weight=16e-12 * 0.07 / 1406247.;
	}
      }
      
      // now multiply by 1fb-1 of lumi
      xsec_weight=xsec_weight*1e15;
      
      
      // loop over theTree entries
      vector<double> vec_val;
      for(Long64_t iEnt=0; iEnt<nEntries; iEnt++)
	{
	  theTree->GetEntry(iEnt); 
	  TBranch* br_var=newTree->GetBranch("xsec_weight");
	  br_var->Fill();
	}
      newTree->Write();
      newFile->Close();
      delete newFile;
    }
}
//---------------------------------
//
//---------------------------------
void AddVarToTree::getFiles()
{
  for(size_t i=0;i<fileNames_.size();i++)
    {
      TFile* file = new TFile(fileNames_[i].c_str());
      files_.push_back(file);
    }
}
//---------------------------------
//
//---------------------------------
AddVarToTree::~AddVarToTree(){
  for(size_t i=0;i<files_.size();i++)
    {
      if(files_[i]){
	if(files_[i]->IsOpen())files_[i]->Close();
	delete files_[i];
	files_[i] = 0;
      }
      
    }
}
