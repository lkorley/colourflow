#include <vector>
#include <cmath>
#include <string>

#include "AddVarToTree.hpp"

using namespace std;

void printUsage(const char *exeName)
{
  cout<<"usage: \n\t"<<exeName
      <<"\t -t treeName"<<"\t --add_interesting"
      <<"\t --single_file fileName"<<endl
      <<"----------------------------------"<<endl
      <<"------- arguments -------"<<endl
      <<"----------------------------------"<<endl
      <<"-t tree name <tree name>"<<endl
      <<"--add_interesting (add the variables to tree)"<<endl
      <<"--single_file <file name> (optional)"<<endl
      <<endl;
}


int main(int argc, char **argv)
{
  const char *fwkBaseDir = getenv("FWK");
  if(!fwkBaseDir){
    cout<<"Please set FWK"<<endl
        <<"(tcsh: 'setenv FWK /<your base directory>/h2bb' )"<<endl;
    return 1;
  }
  cout<<"Using FWK: '"<<fwkBaseDir<<"'"<<endl;

  // path to file and tree
  char treeName[512]="T";
  char singleFileName[512]="";
  char stripPath[512]="";
  bool doInteresting=false;
  if(argc<2){
    printUsage(argv[0]);
    return 1;
  }
  int optind(1);
  
  while ((optind < argc) && (argv[optind][0]=='-')) {
    cout<<"Parsing '"<<argv[optind]<<"'"<<endl;
    std::string sw = argv[optind];
    if(sw == "-t"){
      optind++; 
      sprintf(treeName,"%s",argv[optind]);
    }
    if(sw == "--single_file"){
      optind++;
      sprintf(singleFileName,"%s",argv[optind]);
    }
    if(sw == "--add_interesting"){
      doInteresting=true;
    }
    optind++;
  }
  
  // specify which files to act upon
  vector<const char*> fileNames;
  fileNames.push_back(singleFileName);
  AddVarToTree addVars(fileNames,stripPath,treeName);
  if(doInteresting){
    cout<<"Adding Interesting variables"<<endl;
    addVars.addInterestingVars();
  }
  return 0;
};
