#ifndef AddVarToTree_h
#define AddVarToTree_h

#include <TStyle.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TH2F.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


class AddVarToTree {
  
 private:
  std::vector<std::string> fileNames_;
  std::string path_;
  std::string treeName_;
  std::vector<TFile*> files_;

 public :
  
  AddVarToTree(std::vector<const char*>& fileNames, const char* path, 
	       const char* treeName);
  AddVarToTree(const AddVarToTree& addVarToTree){
    *this=addVarToTree;
  }
  virtual ~AddVarToTree();
  void getFiles();
  void addInterestingVars();
  void addJetEtSystWeight();
  void addMbbSystWeight();
  void addXsecWeight();
  
};
#endif

