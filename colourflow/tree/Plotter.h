//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Dec  3 16:45:16 2016 by ROOT version 6.06/08
// from TTree T/
// found on file: myvartree100.root
//////////////////////////////////////////////////////////

#ifndef Plotter_h
#define Plotter_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class Plotter {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runNumber;
   Double_t        evtNumber;
   UInt_t          nPVs;
   Double_t        j1et;
   Double_t        j2et;
   Double_t        j1eta;
   Double_t        j2eta;
   Double_t        j1deta;
   Double_t        j2deta;
   Double_t        j1rap;
   Double_t        j2rap;
   Double_t        j1ntrks;
   Double_t        j2ntrks;
   Double_t        j1px;
   Double_t        j1py;
   Double_t        j1pz;
   Double_t        j1e;
   Double_t        j2px;
   Double_t        j2py;
   Double_t        j2pz;
   Double_t        j2e;
   Double_t        j1j2dphi;
   Double_t        j1j2drap;
   Double_t        j1j2M;
   Double_t        j1M;
   Double_t        j2M;
   Double_t        j1max_trk_pt;
   Double_t        j2max_trk_pt;
   Double_t        j1min_trk_pt;
   Double_t        j2min_trk_pt;
   Double_t        j1d_trk_pt;
   Double_t        j2d_trk_pt;
   Double_t        j1pull_rap;
   Double_t        j1pull_phi;
   Double_t        j2pull_rap;
   Double_t        j2pull_phi;
   Double_t        j1theta;
   Double_t        j2theta;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_evtNumber;   //!
   TBranch        *b_nPVs;   //!
   TBranch        *b_j1et;   //!
   TBranch        *b_j2et;   //!
   TBranch        *b_j1eta;   //!
   TBranch        *b_j2eta;   //!
   TBranch        *b_j1deta;   //!
   TBranch        *b_j2deta;   //!
   TBranch        *b_j1rap;   //!
   TBranch        *b_j2rap;   //!
   TBranch        *b_j1ntrks;   //!
   TBranch        *b_j2ntrks;   //!
   TBranch        *b_j1px;   //!
   TBranch        *b_j1py;   //!
   TBranch        *b_j1pz;   //!
   TBranch        *b_j1e;   //!
   TBranch        *b_j2px;   //!
   TBranch        *b_j2py;   //!
   TBranch        *b_j2pz;   //!
   TBranch        *b_j2e;   //!
   TBranch        *b_j1j2dphi;   //!
   TBranch        *b_j1j2drap;   //!
   TBranch        *b_j1j2M;   //!
   TBranch        *b_j1M;   //!
   TBranch        *b_j2M;   //!
   TBranch        *b_j1max_trk_pt;   //!
   TBranch        *b_j2max_trk_pt;   //!
   TBranch        *b_j1min_trk_pt;   //!
   TBranch        *b_j2min_trk_pt;   //!
   TBranch        *b_j1d_trk_pt;   //!
   TBranch        *b_j2d_trk_pt;   //!
   TBranch        *b_j1pull_rap;   //!
   TBranch        *b_j1pull_phi;   //!
   TBranch        *b_j2pull_rap;   //!
   TBranch        *b_j2pull_phi;   //!
   TBranch        *b_j1theta;   //!
   TBranch        *b_j2theta;   //!

   Plotter(TTree *tree=0);
   virtual ~Plotter();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Plotter_cxx
Plotter::Plotter(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("myvartree100.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("myvartree100.root");
      }
      f->GetObject("T",tree);

   }
   Init(tree);
}

Plotter::~Plotter()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Plotter::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Plotter::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Plotter::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("evtNumber", &evtNumber, &b_evtNumber);
   fChain->SetBranchAddress("nPVs", &nPVs, &b_nPVs);
   fChain->SetBranchAddress("j1et", &j1et, &b_j1et);
   fChain->SetBranchAddress("j2et", &j2et, &b_j2et);
   fChain->SetBranchAddress("j1eta", &j1eta, &b_j1eta);
   fChain->SetBranchAddress("j2eta", &j2eta, &b_j2eta);
   fChain->SetBranchAddress("j1deta", &j1deta, &b_j1deta);
   fChain->SetBranchAddress("j2deta", &j2deta, &b_j2deta);
   fChain->SetBranchAddress("j1rap", &j1rap, &b_j1rap);
   fChain->SetBranchAddress("j2rap", &j2rap, &b_j2rap);
   fChain->SetBranchAddress("j1ntrks", &j1ntrks, &b_j1ntrks);
   fChain->SetBranchAddress("j2ntrks", &j2ntrks, &b_j2ntrks);
   fChain->SetBranchAddress("j1px", &j1px, &b_j1px);
   fChain->SetBranchAddress("j1py", &j1py, &b_j1py);
   fChain->SetBranchAddress("j1pz", &j1pz, &b_j1pz);
   fChain->SetBranchAddress("j1e", &j1e, &b_j1e);
   fChain->SetBranchAddress("j2px", &j2px, &b_j2px);
   fChain->SetBranchAddress("j2py", &j2py, &b_j2py);
   fChain->SetBranchAddress("j2pz", &j2pz, &b_j2pz);
   fChain->SetBranchAddress("j2e", &j2e, &b_j2e);
   fChain->SetBranchAddress("j1j2dphi", &j1j2dphi, &b_j1j2dphi);
   fChain->SetBranchAddress("j1j2drap", &j1j2drap, &b_j1j2drap);
   fChain->SetBranchAddress("j1j2M", &j1j2M, &b_j1j2M);
   fChain->SetBranchAddress("j1M", &j1M, &b_j1M);
   fChain->SetBranchAddress("j2M", &j2M, &b_j2M);
   fChain->SetBranchAddress("j1max_trk_pt", &j1max_trk_pt, &b_j1max_trk_pt);
   fChain->SetBranchAddress("j2max_trk_pt", &j2max_trk_pt, &b_j2max_trk_pt);
   fChain->SetBranchAddress("j1min_trk_pt", &j1min_trk_pt, &b_j1min_trk_pt);
   fChain->SetBranchAddress("j2min_trk_pt", &j2min_trk_pt, &b_j2min_trk_pt);
   fChain->SetBranchAddress("j1d_trk_pt", &j1d_trk_pt, &b_j1d_trk_pt);
   fChain->SetBranchAddress("j2d_trk_pt", &j2d_trk_pt, &b_j2d_trk_pt);
   fChain->SetBranchAddress("j1pull_rap", &j1pull_rap, &b_j1pull_rap);
   fChain->SetBranchAddress("j1pull_phi", &j1pull_phi, &b_j1pull_phi);
   fChain->SetBranchAddress("j2pull_rap", &j2pull_rap, &b_j2pull_rap);
   fChain->SetBranchAddress("j2pull_phi", &j2pull_phi, &b_j2pull_phi);
   fChain->SetBranchAddress("j1theta", &j1theta, &b_j1theta);
   fChain->SetBranchAddress("j2theta", &j2theta, &b_j2theta);
   Notify();
}

Bool_t Plotter::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Plotter::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Plotter::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Plotter_cxx
